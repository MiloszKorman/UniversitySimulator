package tools;

import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

public class Alerts {

    //Erases red highlight on fields whenever data is provided again
    public static void resetRedAlert(TextField fieldToReset) {

        if (fieldToReset.getStyleClass().contains("error")) {
            fieldToReset.getStyleClass().remove("error");
        }

    }

    //Adds red highlight to fields whenever incorrect data is provided
    public static void addRedAlert(TextField fieldToCheck) {

        if (!fieldToCheck.getStyleClass().contains("error")) {
            fieldToCheck.getStyleClass().add("error");
        }

    }

    //pops up an alert that user hasn't selected necessary object
    public static void noneSelectedAlert(String what) {

        Alert noSelect = new Alert(Alert.AlertType.WARNING);
        noSelect.setTitle("Illegal request");
        noSelect.setHeaderText("No " + what + " has been selected");
        noSelect.showAndWait();

    }

    //whenever something goes wrong and object is lost between views
    public static void lostObjectAlert(String what) {

        Alert noValue = new Alert(Alert.AlertType.ERROR);
        noValue.setTitle("Unexpected error");
        noValue.setHeaderText("Somehow no " + what + " has been passed");
        noValue.showAndWait();

    }

    //whenever something goes wrong and wrong object is passed between views
    public static void wrongObjectAlert(String what) {

        Alert notCorrectObject = new Alert(Alert.AlertType.ERROR);
        notCorrectObject.setTitle("Unexpected error");
        notCorrectObject.setHeaderText("Somehow not " + what + " ended up in here");
        notCorrectObject.showAndWait();

    }

}