package tools;

import GUI.UniversitySimulator;
import models.institutions.University;

import java.io.*;

public class Saves {

    //takes name provided by user and finds out what name of the save would be, then tries to import said save
    public static boolean importSave(String universityName) {

        String[] saveName = universityName.split("\\s+");
        String realSaveName = "";

        for (String partOfName : saveName) {

            realSaveName += partOfName;

        }

        realSaveName += ".uni";

        try {

            ObjectInputStream objectStream = new ObjectInputStream(new FileInputStream(realSaveName));
            UniversitySimulator.runningUniversity = (University) objectStream.readObject();
            objectStream.close();

        } catch (FileNotFoundException fnfe) {

            System.out.println("There is no such file");

            return false;

        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    //based on University's name creates a save name, then saves university with all its properties and affiliations
    public static boolean saveEntry(String universityName) {

        String[] saveName = universityName.split("\\s+");
        String realSaveName = "";

        for (String partOfName : saveName) {

            realSaveName += partOfName;

        }

        realSaveName += ".uni";

        try {

            ObjectOutputStream objectStream = new ObjectOutputStream(new FileOutputStream(realSaveName));
            objectStream.writeObject(UniversitySimulator.runningUniversity);
            objectStream.flush();
            objectStream.close();

        } catch (FileNotFoundException fnfe) {

            System.out.println("There is no such file");//?

            return false;
        } catch (Exception e) {

            e.printStackTrace();

            return false;
        }

        return true;
    }

}
