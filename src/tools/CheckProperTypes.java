package tools;

import javafx.scene.control.TextField;

public class CheckProperTypes {

    //checks if provided album number is correct (length and whether is a number)
    public static boolean checkAlbumNumber(final String albumNumberToCheck) {

        try {

            Integer.parseInt(albumNumberToCheck);

            if (albumNumberToCheck.length() != 6) {
                return false;
            }

            return true;

        } catch (NumberFormatException nfe) {
            return false;
        }

    }

    //checks if provided pesel is correct (length, month, day in said month, and whether is a number)
    public static boolean checkPesel(final String peselToCheck) {

        try {

            long pesel = Long.parseLong(peselToCheck);
            int day = (int) (pesel / (int) Math.pow(10, 5)) % 100;
            int month = (int) (pesel / (int) Math.pow(10, 7)) % 100;
            int year = (int) pesel / (int) Math.pow(10, 9);

            if (month < 1 || 12 < month) {
                System.out.println("Incorrect month");
                return false;
            }

            if (month == 2) {

                if (17 < year) {
                    year += 1900;
                } else {
                    year += 2000;
                }

                if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {

                    if (29 < day) {
                        System.out.println("More than 29 days");
                        return false;
                    }

                } else if (28 < day) {
                    System.out.println("More than 28 days");
                    return false;
                }

                //Other way around
            } else if ((month == 4 || month == 6 || month == 9 || month == 11) && 30 < day) {
                System.out.println("More than 30 days");
                return false;
            } else if (31 < day) {
                System.out.println("More than 31 days");
                return false;
            }

            return true;

        } catch (NumberFormatException nfe) {
            System.out.println("Not a number");
            return false;
        }

    }

    public static double getWage(final TextField wageInput) {

        double wage;

        try {

            wage = Double.parseDouble(wageInput.getText());

            if(wage < 1000 || 11000 < wage) {

                wage = -1;
                Alerts.addRedAlert(wageInput);
                wageInput.clear();
                wageInput.setPromptText("It has to be a number between 1000 and 11000");

            }

        } catch (NumberFormatException nfe) {

            wage = -1;
            Alerts.addRedAlert(wageInput);
            wageInput.clear();
            wageInput.setPromptText("It has to be a number between 1000 and 11000");

        }

        return wage;
    }

    public static int getInteger(final TextField valueInput, final int minVal, final int maxVal) {

        int value;

        try {

            value = Integer.parseInt(valueInput.getText());

            if(value < minVal || maxVal < value) {

                value = -1;
                Alerts.addRedAlert(valueInput);
                valueInput.clear();
                valueInput.setPromptText("It has to be an integer between " + minVal + " and " + maxVal);

            }

        } catch (NumberFormatException nfe) {

            value = -1;
            Alerts.addRedAlert(valueInput);
            valueInput.clear();
            valueInput.setPromptText("It has to be an integer between " + minVal + " and " + maxVal);

        }

        return value;
    }

}
