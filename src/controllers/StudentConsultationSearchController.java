package controllers;

import GUI.UniversitySimulator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.activities.Consultations;
import models.people.ResearcherLecturer;
import models.people.UniversityEmployee;
import tools.Alerts;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class StudentConsultationSearchController implements Initializable {

    public Button backButton;
    public Button signUpButton;
    public TableView<ResearcherLecturer> lecturersList;
    private final ObservableList<ResearcherLecturer> lecturerData = FXCollections.observableArrayList();
    public TableView<Consultations> lecturersConsultationsTable;
    private final ObservableList<Consultations> lecturersConsultationData = FXCollections.observableArrayList();

    public TableColumn<ResearcherLecturer, String> lecturerColumn;
    public TableColumn<Consultations, String> dayColumn;
    public TableColumn<Consultations, String> timeColumn;
    public TableColumn<Consultations, String> timeLeftColumn;

    private ResearcherLecturer lecturerToView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        parseLecturersList();
        parseLecturersConsultationsTable();

    }

    //when certain consultation is selected, displays new window in which student can provide necessary info for sign up
    public void signMeUp() throws IOException {

        Consultations consultationForSignUp = lecturersConsultationsTable.getSelectionModel().getSelectedItem();

        if (consultationForSignUp == null) {

            Alerts.noneSelectedAlert("consultation");

        } else {

            UniversitySimulator.consultationBuffer = consultationForSignUp;

            Stage newConsultationSignUpWindow = new Stage();
            newConsultationSignUpWindow.initModality(Modality.APPLICATION_MODAL);
            Parent consultationSignUp = FXMLLoader.load(getClass().getResource("../views/student-consultation-sign-up.fxml"));
            newConsultationSignUpWindow.setTitle("Consultation Sign Up");
            newConsultationSignUpWindow.setScene(new Scene(consultationSignUp));
            newConsultationSignUpWindow.setResizable(false);
            newConsultationSignUpWindow.showAndWait();

            parseLecturersConsultationsTable();

        }

    }

    //takes list of lecturers from university and displays them
    private void parseLecturersList() {

        ArrayList<ResearcherLecturer> listOfLecturers = new ArrayList<>();

        for (UniversityEmployee employee : UniversitySimulator.runningUniversity.getListOfEmployees()) {

            if (employee instanceof ResearcherLecturer) {

                listOfLecturers.add((ResearcherLecturer) employee);

            }

        }

        lecturerData.setAll(listOfLecturers);
        lecturersList.setItems(lecturerData);
        lecturerColumn.setCellValueFactory(new PropertyValueFactory<>("fullName"));

    }

    //when lecturer is selected, his consultations are displayed
    public void displayLecturersConsultations() {

        lecturerToView = lecturersList.getSelectionModel().getSelectedItem();
        parseLecturersConsultationsTable();

    }

    //takes list of selected lecturers consultations and displays their info
    private void parseLecturersConsultationsTable() {

        if (lecturerToView != null) {

            lecturersConsultationData.setAll(lecturerToView.getMyConsultations());
            lecturersConsultationsTable.setItems(lecturersConsultationData);
            dayColumn.setCellValueFactory(new PropertyValueFactory<>("day"));
            timeColumn.setCellValueFactory(new PropertyValueFactory<>("time"));
            timeLeftColumn.setCellValueFactory(new PropertyValueFactory<>("timeLeft"));

        }

    }

    //returns to the previous view which is
    public void goBack() throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("../views/student-consultation-administration.fxml"));
        backButton.getScene().setRoot(root);

    }

}
