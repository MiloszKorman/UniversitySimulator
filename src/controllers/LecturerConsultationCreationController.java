package controllers;

import GUI.UniversitySimulator;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.activities.Consultations;
import models.people.Person;
import models.people.ResearcherLecturer;
import tools.Alerts;
import tools.CheckProperTypes;

import java.net.URL;
import java.util.ResourceBundle;

public class LecturerConsultationCreationController implements Initializable {

    public Button submitButton;
    public TextField dayInput;
    public TextField hourInput;
    public TextField minuteInput;
    public TextField durationInput;
    private int hourVal;
    private int minuteVal;
    private int durationVal;

    private ResearcherLecturer lecturerForNewConsultation;

    //takes lecturer selected in previous view
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Person forNewConsultation = UniversitySimulator.awaitingBeingManaged;
        UniversitySimulator.awaitingBeingManaged = null;

        if (forNewConsultation == null) {

            Alerts.lostObjectAlert("lecturer");
            ((Stage) submitButton.getScene().getWindow()).close();

        } else {

            if (forNewConsultation instanceof ResearcherLecturer) {
                lecturerForNewConsultation = (ResearcherLecturer) forNewConsultation;
            } else {
                Alerts.wrongObjectAlert("lecturer");
                ((Stage) submitButton.getScene().getWindow()).close();
            }

        }

    }

    //if provided data is correct, creates new consultation hour and adds to lecturers list
    public void submitData() {

        if (checkAll()) {

            lecturerForNewConsultation.addConsultation(new Consultations(dayInput.getText(), hourVal, minuteVal, durationVal, lecturerForNewConsultation));

            ((Stage) submitButton.getScene().getWindow()).close();

        }

    }

    //checks whether all fields are filled with correct data
    private boolean checkAll() {

        boolean allGood = true;

        if (dayInput.getText().isEmpty() || dayInput.getText().split("\\s+").length != 1) {
            Alerts.addRedAlert(dayInput);
            allGood = false;
        }

        if (hourInput.getText().isEmpty()) {

            Alerts.addRedAlert(hourInput);
            allGood = false;

        } else {

            hourVal = CheckProperTypes.getInteger(hourInput, 7, 21);

            if (hourVal == -1) {
                allGood = false;
            }

        }

        if (minuteInput.getText().isEmpty()) {

            Alerts.addRedAlert(minuteInput);
            allGood = false;

        } else {

            minuteVal = CheckProperTypes.getInteger(minuteInput, 0, 59);

            if (minuteVal == -1) {
                allGood = false;
            }

        }

        if (durationInput.getText().isEmpty()) {

            Alerts.addRedAlert(durationInput);
            allGood = false;

        } else {

            durationVal = CheckProperTypes.getInteger(durationInput, 15, 120);

            if (durationVal == -1) {
                allGood = false;
            }

        }

        return allGood;
    }

    //methods for each textfield to remove red glow on click
    public void resetRedAlertDay() {
        Alerts.resetRedAlert(dayInput);
    }

    public void resetRedAlertHour() {
        Alerts.resetRedAlert(hourInput);
    }

    public void resetRedAlertMinute() {
        Alerts.resetRedAlert(minuteInput);
    }

    public void resetRedAlertDuration() {
        Alerts.resetRedAlert(durationInput);
    }

}
