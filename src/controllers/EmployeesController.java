package controllers;

import GUI.UniversitySimulator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.people.ResearcherLecturer;
import models.people.UniversityEmployee;
import tools.Alerts;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class EmployeesController implements Initializable {

    public Button backButton;
    public Button newButton;
    public Button editButton;
    public Button manageConsultationsButton;
    public Button manageOrganizationsButton;
    public Button deleteButton;

    public TableView<UniversityEmployee> employeeTableView;
    private final ObservableList<UniversityEmployee> data = FXCollections.observableArrayList();

    public TableColumn<UniversityEmployee, ImageView> happinessLevelColumn;
    public TableColumn<UniversityEmployee, String> nameColumn;
    public TableColumn<UniversityEmployee, String> peselColumn;
    public TableColumn<UniversityEmployee, String> wageColumn;
    public TableColumn<UniversityEmployee, String> departmentColumn;
    public TableColumn<UniversityEmployee, ImageView> isLecturerColumn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        parseEmployeeList();

    }

    //takes list of employees from university and displays it
    private void parseEmployeeList() {

        data.setAll(UniversitySimulator.runningUniversity.getListOfEmployees());
        employeeTableView.setItems(data);
        happinessLevelColumn.setCellValueFactory(new PropertyValueFactory<>("happinessLevel"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("fullName"));
        peselColumn.setCellValueFactory(new PropertyValueFactory<>("pesel"));
        wageColumn.setCellValueFactory(new PropertyValueFactory<>("wage"));
        departmentColumn.setCellValueFactory(new PropertyValueFactory<>("department"));
        isLecturerColumn.setCellValueFactory(new PropertyValueFactory<>("isLecturer"));

    }

    //opens up window for employee creation then updates list
    public void createNewEmployee() throws IOException {

        Stage newEmployeeWindow = new Stage();
        newEmployeeWindow.initModality(Modality.APPLICATION_MODAL);
        Parent employeeCreation = FXMLLoader.load(getClass().getResource("../views/employee-creation.fxml"));
        newEmployeeWindow.setTitle("New employee");
        newEmployeeWindow.setScene(new Scene(employeeCreation));
        newEmployeeWindow.setResizable(false);
        newEmployeeWindow.showAndWait();
        parseEmployeeList();

    }

    //if employee has been selected opens up new window in which user can edit said employees information
    public void editEmployee() throws IOException {

        UniversityEmployee employeeToEdit = employeeTableView.getSelectionModel().getSelectedItem();

        if (employeeToEdit == null) {

            Alerts.noneSelectedAlert("employee");

        } else {

            UniversitySimulator.awaitingBeingManaged = employeeToEdit;

            Stage editEmployeeWindow = new Stage();
            editEmployeeWindow.initModality(Modality.APPLICATION_MODAL);
            Parent employeeEdit = FXMLLoader.load(getClass().getResource("../views/employee-edit.fxml"));
            editEmployeeWindow.setTitle("Edit employees info");
            editEmployeeWindow.setScene(new Scene(employeeEdit));
            editEmployeeWindow.setResizable(false);
            editEmployeeWindow.showAndWait();
            parseEmployeeList();

        }

    }

    //if a lecturer has been selected, goes to view within which user is able to manage said consultations
    public void manageLecturersConsultations() throws IOException {

        UniversityEmployee employeeToManage = employeeTableView.getSelectionModel().getSelectedItem();

        if (employeeToManage == null) {

            Alerts.noneSelectedAlert("lecturer");

        } else {

            if (employeeToManage instanceof ResearcherLecturer) {

                UniversitySimulator.awaitingBeingManaged = employeeToManage;

                Parent root = FXMLLoader.load(getClass().getResource("../views/lecturer-consultation-administration.fxml"));
                manageConsultationsButton.getScene().setRoot(root);

            } else {

                Alert notALecturer = new Alert(Alert.AlertType.WARNING);
                notALecturer.setTitle("Illegal request");
                notALecturer.setHeaderText("You can only manage Lecturers consultations!");
                notALecturer.showAndWait();

            }

        }

    }

    //if a lecturer has been selected, goes to view within which user is able to manage said organizations
    public void manageLecturersOrganizations() throws IOException {

        UniversityEmployee employeeToManage = employeeTableView.getSelectionModel().getSelectedItem();

        if (employeeToManage == null) {

            Alerts.noneSelectedAlert("lecturer");

        } else {

            if (employeeToManage instanceof ResearcherLecturer) {

                UniversitySimulator.awaitingBeingManaged = employeeToManage;

                Parent root = FXMLLoader.load(getClass().getResource("../views/lecturer-organization-administration.fxml"));
                manageOrganizationsButton.getScene().setRoot(root);

            } else {

                Alert notALecturer = new Alert(Alert.AlertType.WARNING);
                notALecturer.setTitle("Illegal request");
                notALecturer.setHeaderText("You can only manage Lecturers organizations!");
                notALecturer.showAndWait();

            }

        }

    }

    //if an employee has been selected, removes said from University
    public void deleteEmployee() {

        UniversityEmployee employeeToRemove = employeeTableView.getSelectionModel().getSelectedItem();

        if (employeeToRemove == null) {
            Alerts.noneSelectedAlert("employee");
        } else {
            UniversitySimulator.runningUniversity.removeEmployee(employeeToRemove);
        }

    }

    //returns to previous view which is main screen
    public void goBack() throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("../views/main-screen.fxml"));
        backButton.getScene().setRoot(root);

    }

}
