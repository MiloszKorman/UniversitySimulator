package controllers;

import GUI.UniversitySimulator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import models.activities.Organization;
import models.people.Person;
import models.people.Student;
import tools.Alerts;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class StudentOrganizationSignUpController implements Initializable {

    public Button backButton;
    public TableView<Organization> organizationsTable;
    private final ObservableList<Organization> data = FXCollections.observableArrayList();
    public TableColumn<Organization, String> nameColumn;
    public TableColumn<Organization, String> fieldColumn;

    private Student studentToManage;

    //takes selected student from previous view
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Person toManage = UniversitySimulator.awaitingBeingManaged;

        if (toManage == null) {

            Alerts.lostObjectAlert("student");
            backButton.fire();

        } else {

            if (toManage instanceof Student) {
                studentToManage = (Student) toManage;
                parseOrganizationsList();
            } else {
                Alerts.wrongObjectAlert("student");
                backButton.fire();
            }

        }

    }

    //if organization has been selected, adds said organization to students list, and him to organizations list
    public void signUpForOrganization() {

        Organization organizationToSignUpFor = organizationsTable.getSelectionModel().getSelectedItem();

        if (organizationToSignUpFor == null) {

            Alerts.noneSelectedAlert("organization");

        } else {

            if (studentToManage.getOrganizationsImSignedUpFor().contains(organizationToSignUpFor)) {

                Alert alreadyAMember = new Alert(Alert.AlertType.WARNING);
                alreadyAMember.setTitle("Illegal request");
                alreadyAMember.setHeaderText("Student is already a member of this organization");
                alreadyAMember.showAndWait();

            } else {

                studentToManage.addOrganization(organizationToSignUpFor);
                organizationToSignUpFor.signStudentUp(studentToManage);
                backButton.fire();

            }

        }

    }

    //takes list of organizations from University and displays it
    private void parseOrganizationsList() {

        data.setAll(UniversitySimulator.runningUniversity.getListOfOrganizations());
        organizationsTable.setItems(data);
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        fieldColumn.setCellValueFactory(new PropertyValueFactory<>("field"));

    }

    //returns to the previous view which is students organizations administration, needs to pass student,
    //since said is also required in this view
    public void goBack() throws IOException {

        UniversitySimulator.awaitingBeingManaged = studentToManage;

        Parent root = FXMLLoader.load(getClass().getResource("../views/student-organization-administration.fxml"));
        backButton.getScene().setRoot(root);

    }

}
