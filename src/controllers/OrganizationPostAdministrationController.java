package controllers;

import GUI.UniversitySimulator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.activities.Organization;
import models.activities.Post;
import tools.Alerts;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class OrganizationPostAdministrationController implements Initializable {

    public Button backButton;
    public TableView<Post> organizationsPostsTable;
    private final ObservableList<Post> data = FXCollections.observableArrayList();
    public TableColumn<Post, String> titleColumn;
    public TableColumn<Post, String> impactColumn;

    public Button createPostButton;

    private Organization organizationToManage;

    //takes organization that has been selected in previous view
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        organizationToManage = UniversitySimulator.organizationBuffer;
        UniversitySimulator.organizationBuffer = null;

        if (organizationToManage == null) {
            Alerts.lostObjectAlert("organization");
            backButton.fire();
        } else {
            parsePostsList();
        }

    }

    //takes list of posts from organization and displays it
    private void parsePostsList() {

        data.setAll(organizationToManage.getListOfPosts());
        organizationsPostsTable.setItems(data);

        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        impactColumn.setCellValueFactory(new PropertyValueFactory<>("impact"));

    }

    //passes organization in which new post will be created and pops a window with post creation up
    public void createNewPost() throws IOException {

        UniversitySimulator.organizationBuffer = organizationToManage;

        Stage newPostWindow = new Stage();
        newPostWindow.initModality(Modality.APPLICATION_MODAL);
        Parent postCreation = FXMLLoader.load(getClass().getResource("../views/organization-post-creation.fxml"));
        newPostWindow.setTitle("New post");
        newPostWindow.setScene(new Scene(postCreation));
        newPostWindow.setResizable(false);
        newPostWindow.showAndWait();

        parsePostsList();

    }

    //pops new window with posts message
    public void showMessage() throws IOException {

        Post postToShow = organizationsPostsTable.getSelectionModel().getSelectedItem();

        if (postToShow == null) {

            Alerts.noneSelectedAlert("post");

        } else {

            UniversitySimulator.postBuffer = postToShow;

            Stage showPostsContent = new Stage();
            showPostsContent.initModality(Modality.APPLICATION_MODAL);
            Parent showPostsContentView = FXMLLoader.load(getClass().getResource("../views/organizations-posts-content.fxml"));
            showPostsContent.setTitle("Message");
            showPostsContent.setScene(new Scene(showPostsContentView));
            showPostsContent.setResizable(false);
            showPostsContent.showAndWait();

        }

    }

    //returns to previous view which is lecturers organizations administration
    public void goBack() throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("../views/lecturer-organization-administration.fxml"));
        backButton.getScene().setRoot(root);

    }

}
