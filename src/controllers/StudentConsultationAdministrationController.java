package controllers;

import GUI.UniversitySimulator;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.activities.AttendeeInfo;
import models.activities.Consultations;
import models.people.Person;
import models.people.Student;
import tools.Alerts;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class StudentConsultationAdministrationController implements Initializable {

    public Button backButton;
    public Button newConsultationButton;
    public Button editConsultationButton;
    public TableView<AttendeeInfo> myConsultationsInfo;
    private final ObservableList<AttendeeInfo> data = FXCollections.observableArrayList(); //how to handle that?

    public TableColumn<AttendeeInfo, String> lecturerColumn;
    public TableColumn<AttendeeInfo, String> dayColumn;
    public TableColumn<AttendeeInfo, String> timeColumn;
    public TableColumn<AttendeeInfo, String> subjectColumn;
    public TableColumn<AttendeeInfo, String> myTimeColumn;

    private Student studentToManage;

    //takes student that has been selected in previous view
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Person toManage = UniversitySimulator.awaitingBeingManaged;
        UniversitySimulator.awaitingBeingManaged = null;

        if (toManage == null) {

            Alerts.lostObjectAlert("student");
            backButton.fire();

        } else {

            if (toManage instanceof Student) {
                studentToManage = (Student) toManage;
                parseAttendeeList();
            } else {
                Alerts.wrongObjectAlert("lecturer");
                backButton.fire();
            }

        }

    }

    //takes students consultations and his info within them, then displays them
    private void parseAttendeeList() {

        ArrayList<AttendeeInfo> studentsList = new ArrayList<>();

        for (Consultations consultation : studentToManage.getConsultationsIAttend()) {

            studentsList.add(consultation.getMyAttendeeInfo(studentToManage));

        }

        data.setAll(studentsList);
        myConsultationsInfo.setItems(data);
        //think it's better and cleaner than to do all those Consultations getters for PropertyValueFactory in AttendeeInfo
        lecturerColumn.setCellValueFactory(cellData -> (new ReadOnlyStringWrapper(cellData.getValue().getConsultationsOwner().getLecturerOwner().getFullName()).getReadOnlyProperty()));
        dayColumn.setCellValueFactory(cellData -> (new ReadOnlyStringWrapper(cellData.getValue().getConsultationsOwner().getDay()).getReadOnlyProperty()));
        timeColumn.setCellValueFactory(cellData -> (new ReadOnlyStringWrapper(cellData.getValue().getConsultationsOwner().getTime()).getReadOnlyProperty()));
        subjectColumn.setCellValueFactory(cellData -> (new ReadOnlyStringWrapper(cellData.getValue().getConsultationsOwner().getLecturerOwner().getSubject()).getReadOnlyProperty()));
        myTimeColumn.setCellValueFactory(new PropertyValueFactory<>("myTime"));

    }

    //pops up a window which allows user to search for a consultation hour
    public void newConsultation() throws IOException {

        UniversitySimulator.awaitingBeingManaged = studentToManage;

        Parent root = FXMLLoader.load(getClass().getResource("../views/student-consultation-search.fxml"));
        newConsultationButton.getScene().setRoot(root);

    }

    //takes selected attendee info belonging to certain consultation, then pops a window enabling said editing up
    public void editConsultation() throws IOException {

        AttendeeInfo infoOfConsultationToEdit = myConsultationsInfo.getSelectionModel().getSelectedItem();

        if (infoOfConsultationToEdit == null) {

            Alerts.noneSelectedAlert("consultation");

        } else {

            UniversitySimulator.awaitingBeingManaged = studentToManage;
            UniversitySimulator.consultationBuffer = infoOfConsultationToEdit.getConsultationsOwner();

            Stage consultationEditWindow = new Stage();
            consultationEditWindow.initModality(Modality.APPLICATION_MODAL);
            Parent studentsConsultationEdit = FXMLLoader.load(getClass().getResource("../views/student-consultation-edit.fxml"));
            consultationEditWindow.setTitle("Students consultation edit");
            consultationEditWindow.setScene(new Scene(studentsConsultationEdit));
            consultationEditWindow.setResizable(false);
            consultationEditWindow.showAndWait();
            parseAttendeeList();

        }

    }

    //takes selected atendee info and signs student out from consultation host
    public void leaveConsultation() {

        AttendeeInfo infoOfConsultationToLeave = myConsultationsInfo.getSelectionModel().getSelectedItem();

        if (infoOfConsultationToLeave == null) {
            Alerts.noneSelectedAlert("consultation");
        } else {
            infoOfConsultationToLeave.getConsultationsOwner().removeStudent(studentToManage);
            studentToManage.removeConsultations(infoOfConsultationToLeave.getConsultationsOwner());
        }

    }

    //returns to the previous view which is students
    public void goBack() throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("../views/students.fxml"));
        backButton.getScene().setRoot(root);

    }

}
