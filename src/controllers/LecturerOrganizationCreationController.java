package controllers;

import GUI.UniversitySimulator;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.activities.Organization;
import models.people.Person;
import models.people.ResearcherLecturer;
import tools.Alerts;

import java.net.URL;
import java.util.ResourceBundle;

public class LecturerOrganizationCreationController implements Initializable {

    public TextField nameInput;
    public TextField fieldInput;
    public Button submitButton;

    private ResearcherLecturer lecturerForNewOrganization;

    //takes lecturer selected in previous view
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Person toManage = UniversitySimulator.awaitingBeingManaged;
        UniversitySimulator.awaitingBeingManaged = null;

        if (toManage == null) {

            Alerts.lostObjectAlert("lecturer");
            ((Stage) submitButton.getScene().getWindow()).close();

        } else {

            if (toManage instanceof ResearcherLecturer) {
                lecturerForNewOrganization = (ResearcherLecturer) toManage;
            } else {
                Alerts.wrongObjectAlert("lecturer");
                ((Stage) submitButton.getScene().getWindow()).close();
            }

        }

    }

    //if all provided data is correct, then creates new organization, adds it to lecturers and universities lists
    public void submitData() {

        if (checkAll()) {

            Organization organizationToAdd = new Organization(nameInput.getText(), fieldInput.getText(), lecturerForNewOrganization);
            lecturerForNewOrganization.addOrganization(organizationToAdd);
            UniversitySimulator.runningUniversity.addOrganization(organizationToAdd);
            ((Stage) submitButton.getScene().getWindow()).close();

        }

    }

    //checks whether all fields are filled with correct data
    private boolean checkAll() {

        boolean allGood = true;

        if (nameInput.getText().isEmpty()) {
            Alerts.addRedAlert(nameInput);
            allGood = false;
        }

        if (fieldInput.getText().isEmpty()) {
            Alerts.addRedAlert(fieldInput);
            allGood = false;
        }

        return allGood;
    }

    //methods for each textfield to remove red glow on click
    public void resetRedAlertName() {
        Alerts.resetRedAlert(nameInput);
    }

    public void resetRedAlertField() {
        Alerts.resetRedAlert(fieldInput);
    }

}
