package controllers;

import GUI.UniversitySimulator;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.activities.Organization;
import models.activities.Post;
import tools.Alerts;

import java.net.URL;
import java.util.ResourceBundle;

public class OrganizationPostCreationController implements Initializable {

    public TextField titleInput;
    public TextField impactInput;
    public TextArea contentInput;
    public Button submitButton;

    private int impactVal = 0;

    private Organization organizationToManage;

    //takes organization that has been selected in previous view
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        organizationToManage = UniversitySimulator.organizationBuffer;
        UniversitySimulator.organizationBuffer = null;

        if (organizationToManage == null) {
            Alerts.lostObjectAlert("organization");
            ((Stage) submitButton.getScene().getWindow()).close();
        }

    }

    //check if data provided is correct, then creates new post and adds to organizations list
    public void submitData() {

        if (checkAll()) {
            organizationToManage.addPost(new Post(titleInput.getText(), contentInput.getText(), impactVal));
            ((Stage) submitButton.getScene().getWindow()).close();
        }

    }

    //checks whether all of the data provided is correct
    private boolean checkAll() {

        boolean allGood = true;

        if (titleInput.getText().isEmpty()) {
            Alerts.addRedAlert(titleInput);
            allGood = false;
        }

        if (impactInput.getText().isEmpty()) {

            Alerts.addRedAlert(impactInput);
            allGood = false;

        } else {

            try {

                impactVal = Integer.parseInt(impactInput.getText());

                if (impactVal < -100 || 100 < impactVal) {

                    Alerts.addRedAlert(impactInput);
                    allGood = false;

                    impactInput.clear();
                    impactInput.setPromptText("It has to be an integer between -100 and 100");

                }

            } catch (NumberFormatException nfe) {

                Alerts.addRedAlert(impactInput);
                allGood = false;

                impactInput.clear();
                impactInput.setPromptText("It has to be an integer between -100 and 100");

            }

        }

        if (contentInput.getText().isEmpty()) {

            if (!contentInput.getStyleClass().contains("error")) {
                contentInput.getStyleClass().add("error");
            }

        }

        return allGood;
    }

    //methods for each textfield to remove red glow on click
    public void resetRedAlertTitle() {
        Alerts.resetRedAlert(titleInput);
    }

    public void resetRedAlertImpact() {
        Alerts.resetRedAlert(impactInput);
    }

    public void resetRedAlertContent() {

        if (contentInput.getStyleClass().contains("error")) {
            contentInput.getStyleClass().remove("error");
        }

    }

}
