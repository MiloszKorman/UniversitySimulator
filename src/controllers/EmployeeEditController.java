package controllers;

import GUI.UniversitySimulator;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.people.Person;
import models.people.ResearcherLecturer;
import models.people.UniversityEmployee;
import tools.Alerts;
import tools.CheckProperTypes;

import java.net.URL;
import java.util.ResourceBundle;

public class EmployeeEditController implements Initializable {

    public Label nameNSurname;
    public TextField nameNSurnameInput;
    public Label age;
    public TextField ageInput;
    public Label department;
    public TextField departmentInput;
    public Label wage;
    public TextField wageInput;
    public Label subject;
    public TextField subjectInput;
    public Button submitButton;

    private String employeesName = null;
    private String employeesSurname = null;
    private int employeesAge = -1;
    private String employeesDepartment = null;
    private double employeesWage = 0;
    private String lecturersSubject = null;

    private UniversityEmployee employeeToEdit;

    //Takes previously selected employee and operates on him
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Person toEdit = UniversitySimulator.awaitingBeingManaged;
        UniversitySimulator.awaitingBeingManaged = null;

        if (toEdit == null) {

            Alerts.lostObjectAlert("employee");

            ((Stage) submitButton.getScene().getWindow()).close();

        } else {

            if (toEdit instanceof UniversityEmployee) {

                employeeToEdit = (UniversityEmployee) toEdit;

                if (toEdit instanceof ResearcherLecturer) {
                    subject.setDisable(false);
                    subjectInput.setVisible(true);
                }

            } else {

                Alerts.wrongObjectAlert("employee");
                ((Stage) submitButton.getScene().getWindow()).close();

            }

        }

    }


    //if proper value has been entered, such value is set in employees object
    public void submitData() {

        if (checkAll()) {

            if (employeesName != null) {
                employeeToEdit.setName(employeesName);
            }

            if (employeesSurname != null) {
                employeeToEdit.setSurname(employeesSurname);
            }

            if (employeesAge != -1) {
                employeeToEdit.setAge(employeesAge);
            }

            if (employeesDepartment != null) {
                employeeToEdit.setDepartment(employeesDepartment);
            }

            if (employeesWage != 0) {
                employeeToEdit.setWage(employeesWage);
            }

            if (lecturersSubject != null) {
                ((ResearcherLecturer) employeeToEdit).setSubject(lecturersSubject);
            }

            ((Stage) submitButton.getScene().getWindow()).close();

        }

    }

    //when field has been filled, checks whether input is correct, otherwise highlights red
    private boolean checkAll() {

        boolean allGood = true;

        if (!nameNSurnameInput.getText().isEmpty()) {

            if (nameNSurnameInput.getText().split("\\s+").length != 2) {

                Alerts.addRedAlert(nameNSurnameInput);
                allGood = false;

                nameNSurnameInput.clear();
                nameNSurnameInput.setPromptText("It has to consist of two words");

            } else {

                String[] employeesNameNSurname = nameNSurnameInput.getText().split("\\s+");
                employeesName = employeesNameNSurname[0];
                employeesSurname = employeesNameNSurname[1];

            }

        }

        if (!ageInput.getText().isEmpty()) {

            employeesAge = CheckProperTypes.getInteger(ageInput, 21, 100);

            if (employeesAge == -1) {
                allGood = false;
            }

        }

        if (!departmentInput.getText().isEmpty()) {
            employeesDepartment = departmentInput.getText();
        }

        if (!wageInput.getText().isEmpty()) {

            employeesWage = CheckProperTypes.getWage(wageInput);

            if (employeesWage == -1) {
                allGood = false;
            }

        }

        if (employeeToEdit instanceof ResearcherLecturer && !subjectInput.getText().isEmpty()) {
            lecturersSubject = subjectInput.getText();
        }

        return allGood;
    }

    //methods for each textfield to remove red glow on click
    public void resetRedAlertNameNSurname() {
        Alerts.resetRedAlert(nameNSurnameInput);
    }

    public void resetRedAlertAge() {
        Alerts.resetRedAlert(ageInput);
    }

    public void resetRedAlertDepartment() {
        Alerts.resetRedAlert(departmentInput);
    }

    public void resetRedAlertWage() {
        Alerts.resetRedAlert(wageInput);
    }

    public void resetRedAlertSubject() {
        Alerts.resetRedAlert(subjectInput);
    }

}
