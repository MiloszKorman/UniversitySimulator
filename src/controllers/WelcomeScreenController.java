package controllers;

import GUI.UniversitySimulator;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.institutions.University;
import tools.Alerts;
import tools.Saves;

import java.io.IOException;

public class WelcomeScreenController {

    public TextField nameOfUniversity;
    public Button newButton;
    public Button loadButton;

    //if name has been provided, creates new clean University with such name
    public void createNewEntry() throws IOException {

        if (checkCorrectInput()) {

            UniversitySimulator.runningUniversity = new University(nameOfUniversity.getText());

            ((Stage) newButton.getScene().getWindow()).setTitle(nameOfUniversity.getText());

            Parent root = FXMLLoader.load(getClass().getResource("../views/main-screen.fxml"));
            newButton.getScene().setRoot(root);

        }

    }

    //if name has been provided, tries to load University with such name
    public void loadSave() throws IOException {

        if (checkCorrectInput()) {

            if (Saves.importSave(nameOfUniversity.getText())) {

                Parent root = FXMLLoader.load(getClass().getResource("../views/main-screen.fxml"));
                loadButton.getScene().setRoot(root);

            } else {

                Alert noSuchFile = new Alert(Alert.AlertType.WARNING);
                noSuchFile.setTitle("Incorrect data provided");
                noSuchFile.setHeaderText("There is no such file!");
                noSuchFile.showAndWait();

            }

        }

    }

    //checks if any name has been provided, otherwise highlights red
    private boolean checkCorrectInput() {

        if (nameOfUniversity.getText().isEmpty()) {

            Alerts.addRedAlert(nameOfUniversity);

            return false;
        }

        return true;
    }

    //methods for each textfield to remove red glow on click
    public void resetRedAlertUniversityName() {
        Alerts.resetRedAlert(nameOfUniversity);
    }

}