package controllers;

import GUI.UniversitySimulator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.activities.Organization;
import models.activities.Post;
import tools.Alerts;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class OrganizationsPostsController implements Initializable {

    public Button backButton;
    public TableView<Post> postsTable;
    private final ObservableList<Post> data = FXCollections.observableArrayList();
    public TableColumn<Post, String> titleColumn;
    public TableColumn<Post, String> impactColumn;

    Organization organizationToManage;

    //takes organization that has been selected in previous view
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        organizationToManage = UniversitySimulator.organizationBuffer;

        if (organizationToManage == null) {
            Alerts.lostObjectAlert("organization");
            backButton.fire();
        } else {
            parsePostsList();
        }

    }

    //takes list of posts from the organization and displays it
    private void parsePostsList() {

        data.setAll(organizationToManage.getListOfPosts());
        postsTable.setItems(data);

        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        impactColumn.setCellValueFactory(new PropertyValueFactory<>("impact"));

    }

    //displays new window with posts contents
    public void seeMessage() throws IOException {

        Post postToShow = postsTable.getSelectionModel().getSelectedItem();

        if (postToShow == null) {

            Alerts.noneSelectedAlert("post");

        } else {

            UniversitySimulator.postBuffer = postToShow;

            Stage showPostsContent = new Stage();
            showPostsContent.initModality(Modality.APPLICATION_MODAL);
            Parent showPostsContentView = FXMLLoader.load(getClass().getResource("../views/organizations-posts-content.fxml"));
            showPostsContent.setTitle("Message");
            showPostsContent.setScene(new Scene(showPostsContentView));
            showPostsContent.setResizable(false);
            showPostsContent.showAndWait();

        }

    }

    //returns to the previous view which is students organizations administration
    public void goBack() throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("../views/student-organization-administration.fxml"));
        backButton.getScene().setRoot(root);

    }

}
