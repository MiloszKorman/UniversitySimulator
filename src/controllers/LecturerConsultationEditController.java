package controllers;

import GUI.UniversitySimulator;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.activities.Consultations;
import tools.Alerts;
import tools.CheckProperTypes;

import java.net.URL;
import java.util.ResourceBundle;

public class LecturerConsultationEditController implements Initializable {

    public Button submitButton;
    public TextField dayInput;
    public TextField hourInput;
    public TextField minuteInput;
    public TextField durationInput;
    private int hourVal = -1;
    private int minuteVal = -1;
    private int durationVal = -1;

    public Consultations consultationsToEdit;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        consultationsToEdit = UniversitySimulator.consultationBuffer;
        UniversitySimulator.consultationBuffer = null;

        if (consultationsToEdit == null) {
            Alerts.lostObjectAlert("consultations");
            ((Stage) submitButton.getScene().getWindow()).close();
        }


    }

    //checks if provided data is correct, then changes provided details
    public void submitData() {

        if (checkAll()) {

            if (!dayInput.getText().isEmpty()) {
                consultationsToEdit.setDay(dayInput.getText());
            }

            if (hourVal != -1 && minuteVal != -1) {
                consultationsToEdit.setTime(hourVal, minuteVal);
            }

            if (durationVal != -1) {
                consultationsToEdit.setDuration(durationVal);
            }

            ((Stage) submitButton.getScene().getWindow()).close();

        }

    }

    //when field has been filled, checks whether input is correct
    private boolean checkAll() {

        boolean allGood = true;

        //if either hour or minute is defined, but not both at once, then it's a mistake
        if ((!hourInput.getText().isEmpty() || !minuteInput.getText().isEmpty()) && !(!hourInput.getText().isEmpty() && !minuteInput.getText().isEmpty())) {

            if (hourInput.getText().isEmpty()) {

                Alerts.addRedAlert(hourInput);
                allGood = false;

            } else if (minuteInput.getText().isEmpty()) {

                Alerts.addRedAlert(minuteInput);
                allGood = false;

            }

        }

        if (!hourInput.getText().isEmpty()) {

            hourVal = CheckProperTypes.getInteger(hourInput, 7, 21);

            if (hourVal == -1) {
                allGood = false;
            }

        }

        if (!minuteInput.getText().isEmpty()) {

            minuteVal = CheckProperTypes.getInteger(minuteInput, 0, 59);

            if (minuteVal == -1) {
                allGood = false;
            }

        }

        if (!durationInput.getText().isEmpty()) {

            durationVal = CheckProperTypes.getInteger(durationInput, 15, 120);

            if (durationVal == -1) {
                allGood = false;
            }

        }

        return allGood;
    }

    //methods for each textfield to remove red glow on click
    public void resetRedAlertDay() {
        Alerts.resetRedAlert(dayInput);
    }

    public void resetRedAlertHour() {
        Alerts.resetRedAlert(hourInput);
    }

    public void resetRedAlertMinute() {
        Alerts.resetRedAlert(minuteInput);
    }

    public void resetRedAlertDuration() {
        Alerts.resetRedAlert(durationInput);
    }

}
