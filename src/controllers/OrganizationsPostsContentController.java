package controllers;

import GUI.UniversitySimulator;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import models.activities.Post;

import java.net.URL;
import java.util.ResourceBundle;

public class OrganizationsPostsContentController implements Initializable {

    public TextArea postsContent;

    private Post postToShow;

    //takes from buffer post it ought to show and displays its content
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        postToShow = UniversitySimulator.postBuffer;
        UniversitySimulator.postBuffer = null;

        postsContent.setText(postToShow.getMessage());

    }

}
