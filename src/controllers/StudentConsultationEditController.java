package controllers;

import GUI.UniversitySimulator;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.activities.Consultations;
import models.people.Person;
import models.people.Student;
import tools.Alerts;
import tools.CheckProperTypes;

import java.net.URL;
import java.util.ResourceBundle;

public class StudentConsultationEditController implements Initializable {

    public ChoiceBox<String> priority;
    public TextField minTime;
    public TextField maxTime;
    public Button submitButton;
    private int priorityVal = 0;
    private int minTimeVal = 0;
    private int maxTimeVal = 0;

    private Student studentToEditInfo;
    private Consultations consultationToEdit;

    //takes student and his consultation hour selected in previous views
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Person toSignUp = UniversitySimulator.awaitingBeingManaged;
        UniversitySimulator.awaitingBeingManaged = null;

        if (toSignUp == null) {//shouldn't ever happen

            Alerts.lostObjectAlert("student");

            ((Stage) submitButton.getScene().getWindow()).close();

        } else {

            if (toSignUp instanceof Student) {
                studentToEditInfo = (Student) toSignUp;
            } else {
                Alerts.wrongObjectAlert("student");
                ((Stage) submitButton.getScene().getWindow()).close();
            }

        }

        consultationToEdit = UniversitySimulator.consultationBuffer;
        UniversitySimulator.consultationBuffer = null;

        if (consultationToEdit == null) {
            Alerts.lostObjectAlert("consultation");
            ((Stage) submitButton.getScene().getWindow()).close();
        }

    }

    //checks if provided data is valid, then changes provided information in consultation
    public void submitData() {

        if (checkAll()) {

            if (priorityVal != 0) {
                consultationToEdit.getMyAttendeeInfo(studentToEditInfo).setNewPriority(priorityVal, studentToEditInfo);
            }

            if (minTimeVal != 0) {
                consultationToEdit.getMyAttendeeInfo(studentToEditInfo).setNewMinTime(minTimeVal, studentToEditInfo);
            }

            if (maxTimeVal != 0) {
                consultationToEdit.getMyAttendeeInfo(studentToEditInfo).setNewMaxTime(maxTimeVal, studentToEditInfo);
            }

        }

    }

    //when field has been filled, checks whether input is correct
    public boolean checkAll() {

        boolean allGood = true;

        if (priority.getValue() != null) {

            switch (priority.getValue()) {

                case "Low":
                    priorityVal = 1;
                    break;

                case "Medium":
                    priorityVal = 2;
                    break;

                case "High":
                    priorityVal = 3;
                    break;

            }

        }

        if (!minTime.getText().isEmpty()) {

            if (consultationToEdit.getTimeLeft() < consultationToEdit.getDuration() / 4) {

                minTimeVal = CheckProperTypes.getInteger(minTime, 1, consultationToEdit.getTimeLeft());

                if (minTimeVal == -1) {
                    allGood = false;
                }

            } else {

                minTimeVal = CheckProperTypes.getInteger(minTime, 1, consultationToEdit.getDuration() / 4);

                if (minTimeVal == -1) {
                    allGood = false;
                }

            }

        }

        if (!maxTime.getText().isEmpty()) {

            if (consultationToEdit.getTimeLeft() < consultationToEdit.getDuration() / 4) {

                maxTimeVal = CheckProperTypes.getInteger(maxTime, 1, consultationToEdit.getTimeLeft());

                if (maxTimeVal == -1) {
                    allGood = false;
                }

                if (minTimeVal == -1) {

                    if (maxTimeVal < consultationToEdit.getMyAttendeeInfo(studentToEditInfo).getMinTime()) {

                        allGood = false;
                        Alerts.addRedAlert(maxTime);
                        maxTime.clear();
                        maxTime.setPromptText("Max time can't be smaller than " + consultationToEdit.getMyAttendeeInfo(studentToEditInfo).getMinTime());

                    }

                } else {

                    if (maxTimeVal < minTimeVal) {

                        allGood = false;
                        Alerts.addRedAlert(maxTime);
                        maxTime.clear();
                        maxTime.setPromptText("Max time must be at least as big as min time");

                    }

                }

            } else {

                maxTimeVal = CheckProperTypes.getInteger(maxTime, 1, consultationToEdit.getDuration() / 4);

                if (maxTimeVal == -1) {
                    allGood = false;
                }

                if (minTimeVal == -1) {

                    if (maxTimeVal < consultationToEdit.getMyAttendeeInfo(studentToEditInfo).getMinTime()) {

                        allGood = false;
                        Alerts.addRedAlert(maxTime);
                        maxTime.clear();
                        maxTime.setPromptText("Max time can't be smaller than " + consultationToEdit.getMyAttendeeInfo(studentToEditInfo).getMinTime());

                    }

                } else {

                    if (maxTimeVal < minTimeVal) {

                        allGood = false;
                        Alerts.addRedAlert(maxTime);
                        maxTime.clear();
                        maxTime.setPromptText("Max time must be at least as big as min time");

                    }

                }

            }

        }

        return allGood;
    }

    //methods for each textfield to remove red glow on click
    public void resetRedAlertMinTime() {
        Alerts.resetRedAlert(minTime);
    }

    public void resetRedAlertMaxTime() {
        Alerts.resetRedAlert(maxTime);
    }

}
