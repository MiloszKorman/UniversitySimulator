package controllers;

import GUI.UniversitySimulator;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.activities.Consultations;
import models.people.Person;
import models.people.Student;
import tools.Alerts;
import tools.CheckProperTypes;

import java.net.URL;
import java.util.ResourceBundle;

public class StudentConsultationSignUpController implements Initializable {

    public ChoiceBox<String> priority;
    public TextField minTime;
    public TextField maxTime;
    public Button submitButton;
    private int priorityVal = 0;
    private int minTimeVal;
    private int maxTimeVal;

    private Student studentToSignUp;
    private Consultations consultationToSignUpFor;

    //takes selected student and consultation hour from previous views
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Person toSignUp = UniversitySimulator.awaitingBeingManaged;

        if (toSignUp == null) {

            Alerts.lostObjectAlert("student");

            ((Stage) submitButton.getScene().getWindow()).close();

        } else {

            if (toSignUp instanceof Student) {
                studentToSignUp = (Student) toSignUp;
            } else {
                Alerts.wrongObjectAlert("student");
                ((Stage) submitButton.getScene().getWindow()).close();
            }

        }

        consultationToSignUpFor = UniversitySimulator.consultationBuffer;
        UniversitySimulator.consultationBuffer = null;

        if (consultationToSignUpFor == null) {
            Alerts.lostObjectAlert("consultation");
            ((Stage) submitButton.getScene().getWindow()).close();
        }

    }

    //if all data provided is correct, tries to sign up student for consultation
    public void submitData() {

        if (checkAll()) {

            switch (priority.getValue()) {

                case "High":
                    priorityVal = 3;
                    break;

                case "Medium":
                    priorityVal = 2;
                    break;

                case "Low":
                    priorityVal = 1;
                    break;

            }

            //adds to users consultations list only if he has been accepted
            if (consultationToSignUpFor.singUp(studentToSignUp, priorityVal, minTimeVal, maxTimeVal) != null) {

                studentToSignUp.addConsultations(consultationToSignUpFor);
                UniversitySimulator.awaitingBeingManaged = studentToSignUp;

                ((Stage) submitButton.getScene().getWindow()).close();

            }

        }

    }

    //checks whether all fields are filled with correct data
    private boolean checkAll() {

        boolean allGood = true;

        if (priority.getValue() == null) {

            if (!priority.getStyleClass().contains("error")) {

                if (!priority.getScene().getStylesheets().contains(getClass().getClassLoader().getResource("views/styles/text-input-red-border.css"))) {
                    priority.getScene().getStylesheets().add(getClass().getClassLoader().getResource("views/styles/text-input-red-border.css").toExternalForm());
                }

                priority.getStyleClass().add("error");

            }

            allGood = false;

        }

        if (minTime.getText().isEmpty()) {

            Alerts.addRedAlert(minTime);
            allGood = false;

        } else {

            if (consultationToSignUpFor.getTimeLeft() < consultationToSignUpFor.getDuration() / 3) {

                minTimeVal = CheckProperTypes.getInteger(minTime, 1, consultationToSignUpFor.getTimeLeft());

                if (minTimeVal == -1) {
                    allGood = false;
                }

            } else {

                minTimeVal = CheckProperTypes.getInteger(minTime, 1, consultationToSignUpFor.getDuration() / 3);

                if (minTimeVal == -1) {
                    allGood = false;
                }

            }

        }

        if (maxTime.getText().isEmpty()) {

            Alerts.addRedAlert(maxTime);
            allGood = false;

        } else {

            if (consultationToSignUpFor.getTimeLeft() < consultationToSignUpFor.getDuration() / 3) {

                maxTimeVal = CheckProperTypes.getInteger(maxTime, 1, consultationToSignUpFor.getTimeLeft());

                if (maxTimeVal == -1) {
                    allGood = false;
                } else if (maxTimeVal < minTimeVal) {

                    allGood = false;
                    Alerts.addRedAlert(maxTime);
                    maxTime.clear();
                    maxTime.setPromptText("Max time must be at least as big as min time");

                }

            } else {

                maxTimeVal = CheckProperTypes.getInteger(maxTime, 1, consultationToSignUpFor.getDuration() / 3);

                if (maxTimeVal == -1) {
                    allGood = false;
                } else if (maxTimeVal < minTimeVal) {

                    allGood = false;
                    Alerts.addRedAlert(maxTime);
                    maxTime.clear();
                    maxTime.setPromptText("Max time must be at least as big as min time");

                }

            }

        }

        return allGood;
    }

    //methods for each textfield to remove red glow on click
    public void resetRedAlertMinTime() {
        Alerts.resetRedAlert(minTime);
    }

    public void resetRedAlertMaxTime() {
        Alerts.resetRedAlert(maxTime);
    }

}
