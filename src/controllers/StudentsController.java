package controllers;

import GUI.UniversitySimulator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.people.Student;
import tools.Alerts;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class StudentsController implements Initializable {

    public Button backButton;
    public Button newButton;
    public Button editButton;
    public Button manageConsultationsButton;
    public Button manageOrganizationsButton;
    public Button deleteButton;

    public TableView<Student> studentTableView;
    private final ObservableList<Student> data = FXCollections.observableArrayList();

    public TableColumn<Student, ImageView> happinessLevelColumn;
    public TableColumn<Student, String> albumNumberColumn;
    public TableColumn<Student, String> nameColumn;
    public TableColumn<Student, String> ageColumn;
    public TableColumn<Student, String> peselColumn;

    //displays the list on start
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        parseStudentList();

    }

    //takes list of students from University and displays it
    private void parseStudentList() {

        data.setAll(UniversitySimulator.runningUniversity.getListOfStudents());
        studentTableView.setItems(data);
        happinessLevelColumn.setCellValueFactory(new PropertyValueFactory<>("happinessLevel"));
        albumNumberColumn.setCellValueFactory(new PropertyValueFactory<>("studentsAlbumNumber"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("fullName"));
        ageColumn.setCellValueFactory(new PropertyValueFactory<>("age"));
        peselColumn.setCellValueFactory(new PropertyValueFactory<>("pesel"));

    }

    //pops new window up in which user can create new student
    public void createNewStudent() throws IOException {

        Stage newStudentWindow = new Stage();
        newStudentWindow.initModality(Modality.APPLICATION_MODAL);
        Parent studentCreation = FXMLLoader.load(getClass().getResource("../views/student-creation.fxml"));
        newStudentWindow.setTitle("New student");
        newStudentWindow.setScene(new Scene(studentCreation));
        newStudentWindow.setResizable(false);
        newStudentWindow.showAndWait();
        parseStudentList();

    }

    //if student has been selected, new window is opened in which user can edit said information
    public void editStudent() throws IOException {

        Student studentToEdit = studentTableView.getSelectionModel().getSelectedItem();

        if (studentToEdit == null) {

            Alerts.noneSelectedAlert("student");

        } else {

            UniversitySimulator.awaitingBeingManaged = studentToEdit;

            Stage editStudentWindow = new Stage();
            editStudentWindow.initModality(Modality.APPLICATION_MODAL);
            Parent studentEdit = FXMLLoader.load(getClass().getResource("../views/student-edit.fxml"));
            editStudentWindow.setTitle("Edit employees info");
            editStudentWindow.setScene(new Scene(studentEdit));
            editStudentWindow.setResizable(false);
            editStudentWindow.showAndWait();
            parseStudentList();

        }

    }

    //if student has been selected new view is loaded in which user can manage said consultations
    public void manageStudentsConsultations() throws IOException {

        Student studentToManage = studentTableView.getSelectionModel().getSelectedItem();

        if (studentToManage == null) {

            Alerts.noneSelectedAlert("student");

        } else {

            UniversitySimulator.awaitingBeingManaged = studentToManage;

            Parent root = FXMLLoader.load(getClass().getResource("../views/student-consultation-administration.fxml"));
            manageConsultationsButton.getScene().setRoot(root);

        }

    }

    //if student has been selected new view is loaded in which user can manage said organizations
    public void manageStudentsOrganizations() throws IOException {

        Student studentToManage = studentTableView.getSelectionModel().getSelectedItem();

        if (studentToManage == null) {

            Alerts.noneSelectedAlert("student");

        } else {

            UniversitySimulator.awaitingBeingManaged = studentToManage;

            Parent root = FXMLLoader.load(getClass().getResource("../views/student-organization-administration.fxml"));
            manageOrganizationsButton.getScene().setRoot(root);

        }

    }

    //if student has been selected said is removed from Universities list
    public void deleteStudent() {

        Student studentToRemove = studentTableView.getSelectionModel().getSelectedItem();

        if (studentToRemove == null) {
            Alerts.noneSelectedAlert("student");
        } else {
            UniversitySimulator.runningUniversity.removeStudent(studentToRemove);
            parseStudentList();
        }

    }

    //returns to the previous view which is main screen
    public void goBack() throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("../views/main-screen.fxml"));
        backButton.getScene().setRoot(root);

    }

}
