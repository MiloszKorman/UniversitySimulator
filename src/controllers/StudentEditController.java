package controllers;

import GUI.UniversitySimulator;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.people.Person;
import models.people.Student;
import tools.Alerts;
import tools.CheckProperTypes;

import java.net.URL;
import java.util.ResourceBundle;

public class StudentEditController implements Initializable {

    public Label nameNSurname;
    public TextField nameNSurnameInput;
    public Label age;
    public TextField ageInput;
    public Button submitButton;

    private String studentsName = null;
    private String studentsSurname = null;
    private int studentsAge = -1;

    private Student studentToEdit;

    //takes selected student from previous view
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Person toEdit = UniversitySimulator.awaitingBeingManaged;
        UniversitySimulator.awaitingBeingManaged = null;

        if (toEdit == null || !(toEdit instanceof Student)) {
            Alerts.lostObjectAlert("student");
            ((Stage) submitButton.getScene().getWindow()).close();
        } else {
            studentToEdit = (Student) toEdit;
        }

    }

    //if all provided data is correct, changes provided ones in student
    public void submitData() {

        if (checkAll()) {

            if (studentsName != null) {
                studentToEdit.setName(studentsName);
            }

            if (studentsSurname != null) {
                studentToEdit.setSurname(studentsSurname);
            }

            if (studentsAge != -1) {
                studentToEdit.setAge(studentsAge);
            }

        }

    }

    //when field has been filled, checks whether input is correct
    private boolean checkAll() {

        boolean allGood = true;

        if (!nameNSurnameInput.getText().isEmpty()) {

            if (nameNSurnameInput.getText().split("\\s+").length != 2) {

                Alerts.addRedAlert(nameNSurnameInput);
                allGood = false;

            } else {

                String[] employeesNameNSurname = nameNSurnameInput.getText().split("\\s+");
                studentsName = employeesNameNSurname[0];
                studentsSurname = employeesNameNSurname[1];

            }

        }

        if (!ageInput.getText().isEmpty()) {

            studentsAge = CheckProperTypes.getInteger(ageInput, 15, 100);

            if (studentsAge == -1) {
                allGood = false;
            }

        }

        return allGood;
    }

    //methods for each textfield to remove red glow on click
    public void resetRedAlertNameNSurname() {
        Alerts.resetRedAlert(nameNSurnameInput);
    }

    public void resetRedAlertAge() {
        Alerts.resetRedAlert(ageInput);
    }

}
