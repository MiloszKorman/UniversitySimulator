package controllers;

import GUI.UniversitySimulator;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.people.Student;
import tools.CheckProperTypes;
import tools.Alerts;

public class StudentCreationController {

    public Label nameNSurname;
    public TextField nameNSurnameInput;
    public Label age;
    public TextField ageInput;
    public Label pesel;
    public TextField peselInput;
    public Label albumNumber;
    public TextField albumNumberInput;
    public Button submitButton;

    private String studentsName;
    private String studentsSurname;
    private int studentsAge;
    private String studentsPesel;
    private String studentsAlbumNumber;

    //if all provided data is correct, creates new student and adds him to universities list
    public void submitData() {

        if (checkAll()) {
            UniversitySimulator.runningUniversity.addStudent(new Student(studentsName, studentsSurname, studentsAge, studentsPesel, studentsAlbumNumber));
            ((Stage) submitButton.getScene().getWindow()).close();
        }

    }

    //checks whether all fields are filled with correct data
    private boolean checkAll() {

        boolean allGood = true;

        if (nameNSurnameInput.getText().isEmpty() || nameNSurnameInput.getText().split("\\s+").length != 2) {

            Alerts.addRedAlert(nameNSurnameInput);
            allGood = false;

        } else {

            String[] studentsNameNSurname = nameNSurnameInput.getText().split("\\s+");
            studentsName = studentsNameNSurname[0];
            studentsSurname = studentsNameNSurname[1];

        }

        if (ageInput.getText().isEmpty()) {

            Alerts.addRedAlert(ageInput);
            allGood = false;

        } else {

            studentsAge = CheckProperTypes.getInteger(ageInput, 15, 100);

            if (studentsAge == -1) {
                allGood = false;
            }

        }

        if (peselInput.getText().isEmpty() || peselInput.getText().length() != 11 || !CheckProperTypes.checkPesel(peselInput.getText())) {
            Alerts.addRedAlert(peselInput);
            allGood = false;
        } else {
            studentsPesel = peselInput.getText();
        }

        if (albumNumberInput.getText().isEmpty() || !CheckProperTypes.checkAlbumNumber(albumNumberInput.getText())) {
            Alerts.addRedAlert(albumNumberInput);
            allGood = false;
        } else {
            studentsAlbumNumber = albumNumberInput.getText();
        }


        return allGood;
    }

    //methods for each textfield to remove red glow on click
    public void resetRedAlertNameNSurname() {
        Alerts.resetRedAlert(nameNSurnameInput);
    }

    public void resetRedAlertAge() {
        Alerts.resetRedAlert(ageInput);
    }

    public void resetRedAlertPesel() {
        Alerts.resetRedAlert(peselInput);
    }

    public void resetRedAlertAlbumNumber() {
        Alerts.resetRedAlert(albumNumberInput);
    }

}
