package controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;

import java.io.IOException;

public class MainScreenController {

    public StackPane employeesPane;
    public StackPane studentsPane;
    public Button backButton;

    //loads employees view
    public void manageEmployees() throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("../views/employees.fxml"));
        employeesPane.getScene().setRoot(root);

    }

    //loads students view
    public void manageStudents() throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("../views/students.fxml"));
        studentsPane.getScene().setRoot(root);

    }

    //returns to previous view which is welcome screen
    public void goBack() throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("../views/welcome-screen.fxml"));
        backButton.getScene().setRoot(root);

    }

}
