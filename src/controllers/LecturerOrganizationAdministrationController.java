package controllers;

import GUI.UniversitySimulator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.activities.Organization;
import models.people.Person;
import models.people.ResearcherLecturer;
import tools.Alerts;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LecturerOrganizationAdministrationController implements Initializable {

    public Button backButton;
    public TableView<Organization> lecturersOrganizationsTable;
    public TableColumn<Organization, String> nameColumn;
    public TableColumn<Organization, String> fieldColumn;
    private final ObservableList<Organization> data = FXCollections.observableArrayList();
    public Button createOrganization;
    public Button managePosts;
    public Button removeOrganization;

    private ResearcherLecturer lecturerToManage;

    //takes lecturer from buffer, which organizations user can manage
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Person toManage = UniversitySimulator.awaitingBeingManaged;
        UniversitySimulator.awaitingBeingManaged = null;

        if (toManage == null) {

            Alerts.lostObjectAlert("lecturer");
            backButton.fire();

        } else {

            if (toManage instanceof ResearcherLecturer) {
                lecturerToManage = (ResearcherLecturer) toManage;
                parseOrganizationsList();
            } else {
                Alerts.wrongObjectAlert("lecturer");
                backButton.fire();
            }

        }

    }

    //new window pops up in which user can create new organization, then list is updated
    public void createNewOrganization() throws IOException {

        UniversitySimulator.awaitingBeingManaged = lecturerToManage;

        Stage newOrganizationWindow = new Stage();
        newOrganizationWindow.initModality(Modality.APPLICATION_MODAL);
        Parent organizationCreation = FXMLLoader.load(getClass().getResource("../views/lecturer-organization-creation.fxml"));
        newOrganizationWindow.setTitle("New organization");
        newOrganizationWindow.setScene(new Scene(organizationCreation));
        newOrganizationWindow.setResizable(false);
        newOrganizationWindow.showAndWait();

        parseOrganizationsList();

    }

    //if organization has been selected, then new view is loaded in which user can add new posts
    public void manageSelectedOrganizationsPosts() throws IOException {

        Organization organizationToManagePosts = lecturersOrganizationsTable.getSelectionModel().getSelectedItem();

        if (organizationToManagePosts == null) {

            Alerts.noneSelectedAlert("organization");

        } else {

            UniversitySimulator.organizationBuffer = organizationToManagePosts;
            UniversitySimulator.awaitingBeingManaged = lecturerToManage;

            Parent root = FXMLLoader.load(getClass().getResource("../views/organization-post-administration.fxml"));
            managePosts.getScene().setRoot(root);

        }

    }

    //if organizations has been selected, then removes said from lecturers list(during which all students are laid off)
    public void removeSelectedOrganization() {

        Organization organizationToRemove = lecturersOrganizationsTable.getSelectionModel().getSelectedItem();

        if (organizationToRemove == null) {
            Alerts.noneSelectedAlert("organization");
        } else {
            lecturerToManage.removeOrganization(organizationToRemove);
            parseOrganizationsList();
        }

    }

    //takes list of organizations from lecturer and displays it
    private void parseOrganizationsList() {

        data.setAll(lecturerToManage.getMyOrganizations());
        lecturersOrganizationsTable.setItems(data);

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        fieldColumn.setCellValueFactory(new PropertyValueFactory<>("field"));

    }

    //returns to previous view which is employees
    public void goBack() throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("../views/employees.fxml"));
        backButton.getScene().setRoot(root);

    }

}
