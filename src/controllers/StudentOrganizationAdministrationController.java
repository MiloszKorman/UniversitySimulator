package controllers;

import GUI.UniversitySimulator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import models.activities.Organization;
import models.people.Person;
import models.people.Student;
import tools.Alerts;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class StudentOrganizationAdministrationController implements Initializable {

    public Button backButton;
    public Button signUpButton;
    public Button seePostsButton;
    public TableView<Organization> studentsOrganizationsTable;
    private final ObservableList<Organization> data = FXCollections.observableArrayList();
    public TableColumn<Organization, String> nameColumn;
    public TableColumn<Organization, String> fieldColumn;

    Student studentToManage;

    //takes selected user from previous view
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Person toManage = UniversitySimulator.awaitingBeingManaged;
        UniversitySimulator.awaitingBeingManaged = null;

        if (toManage == null) {

            Alerts.lostObjectAlert("student");
            backButton.fire();

        } else {

            if (toManage instanceof Student) {
                studentToManage = (Student) toManage;
                parseOrganizationsList();
            } else {
                Alerts.wrongObjectAlert("student");
                backButton.fire();
            }

        }

    }

    //loads new view in which user can sign student up for new organizations
    public void signUpOrganization() throws IOException {

        UniversitySimulator.awaitingBeingManaged = studentToManage;

        Parent root = FXMLLoader.load(getClass().getResource("../views/student-organization-sign-up.fxml"));
        signUpButton.getScene().setRoot(root);

    }

    //if organization has been selected loads new view in which user can look through said posts
    public void seeOrganizationsPosts() throws IOException {

        Organization organizationToSee = studentsOrganizationsTable.getSelectionModel().getSelectedItem();

        if (organizationToSee == null) {

            Alerts.noneSelectedAlert("organization");

        } else {

            UniversitySimulator.awaitingBeingManaged = studentToManage;
            UniversitySimulator.organizationBuffer = organizationToSee;

            Parent root = FXMLLoader.load(getClass().getResource("../views/organizations-posts.fxml"));
            seePostsButton.getScene().setRoot(root);

        }

    }

    //takes selected organization and lets student sign out
    public void resignOrganization() {

        Organization organizationToResign = studentsOrganizationsTable.getSelectionModel().getSelectedItem();

        if (organizationToResign == null) {
            Alerts.noneSelectedAlert("organization");
        } else {
            organizationToResign.letMeResign(studentToManage);
            studentToManage.removeOrganization(organizationToResign);
            parseOrganizationsList();
        }

    }

    //takes list of organizations from student and displays it
    private void parseOrganizationsList() {

        data.setAll(studentToManage.getOrganizationsImSignedUpFor());
        studentsOrganizationsTable.setItems(data);

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        fieldColumn.setCellValueFactory(new PropertyValueFactory<>("field"));

    }

    //returns to the previous view which is students
    public void goBack() throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("../views/students.fxml"));
        backButton.getScene().setRoot(root);

    }

}
