package controllers;

import GUI.UniversitySimulator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.activities.Consultations;
import models.people.Person;
import models.people.ResearcherLecturer;
import tools.Alerts;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LecturerConsultationAdministrationController implements Initializable {

    public Button backButton;
    public Button createConsultation;
    public TableView<Consultations> employeesConsultationsTable;
    private final ObservableList<Consultations> data = FXCollections.observableArrayList();

    public TableColumn<Consultations, String> dayColumn;
    public TableColumn<Consultations, String> timeColumn;
    public TableColumn<Consultations, String> durationColumn;
    public TableColumn<Consultations, String> timeLeftColumn;
    public TableColumn<Consultations, String> numberOfStudentsColumn;

    private ResearcherLecturer lecturerToManage;

    //Takes lecturer from buffer, which consultations user can manage
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Person toManage = UniversitySimulator.awaitingBeingManaged;
        UniversitySimulator.awaitingBeingManaged = null;

        if (toManage == null) {

            Alerts.lostObjectAlert("lecturer");
            backButton.fire();

        } else {

            if (toManage instanceof ResearcherLecturer) {
                lecturerToManage = (ResearcherLecturer) toManage;
                parseConsultationsList();
            } else {
                Alerts.wrongObjectAlert("lecturer");
                backButton.fire();
            }

        }

    }

    //passes lecturer to new window in which user can create new consultation hour, then list is updated
    public void createNewConsultation() throws IOException {

        UniversitySimulator.awaitingBeingManaged = lecturerToManage;

        Stage newConsultationWindow = new Stage();
        newConsultationWindow.initModality(Modality.APPLICATION_MODAL);
        Parent consultationCreation = FXMLLoader.load(getClass().getResource("../views/lecturer-consultation-creation.fxml"));
        newConsultationWindow.setTitle("New consultation");
        newConsultationWindow.setScene(new Scene(consultationCreation));
        newConsultationWindow.setResizable(false);
        newConsultationWindow.showAndWait();

        parseConsultationsList();

    }

    //if consultation is selected new window pops up in which user can change said information, then list is updated
    public void editSelectedConsultation() throws IOException {

        Consultations consultationForEdit = employeesConsultationsTable.getSelectionModel().getSelectedItem();

        if (consultationForEdit == null) {

            Alerts.noneSelectedAlert("consultation");

        } else {

            UniversitySimulator.consultationBuffer = consultationForEdit;

            Stage consultationEditWindow = new Stage();
            consultationEditWindow.initModality(Modality.APPLICATION_MODAL);
            Parent lecturersConsultationEdit = FXMLLoader.load(getClass().getResource("../views/lecturer-consultation-edit.fxml"));
            consultationEditWindow.setTitle("Lecturers consultation edit");
            consultationEditWindow.setScene(new Scene(lecturersConsultationEdit));
            consultationEditWindow.setResizable(false);
            consultationEditWindow.showAndWait();
            parseConsultationsList();

        }

    }

    //if consultation is selected, said is removed from lecturers list(during which all students are laid off)
    public void removeSelectedConsultation() {

        Consultations consultationToRemove = employeesConsultationsTable.getSelectionModel().getSelectedItem();

        if (consultationToRemove == null) {
            Alerts.noneSelectedAlert("consultation");
        } else {
            lecturerToManage.removeConsultation(consultationToRemove);
            parseConsultationsList();
        }

    }

    //takes list of consultations from lecturer that has been passed
    private void parseConsultationsList() {

        data.setAll(lecturerToManage.getMyConsultations());
        employeesConsultationsTable.setItems(data);
        dayColumn.setCellValueFactory(new PropertyValueFactory<>("day"));
        timeColumn.setCellValueFactory(new PropertyValueFactory<>("time"));
        durationColumn.setCellValueFactory(new PropertyValueFactory<>("duration"));
        timeLeftColumn.setCellValueFactory(new PropertyValueFactory<>("timeLeft"));
        numberOfStudentsColumn.setCellValueFactory(new PropertyValueFactory<>("numberOfStudents"));

    }

    //returns to previous view which is employees
    public void goBack() throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("../views/employees.fxml"));
        backButton.getScene().setRoot(root);

    }

}
