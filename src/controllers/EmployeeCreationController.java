package controllers;

import GUI.UniversitySimulator;
import javafx.scene.control.*;
import javafx.stage.Stage;
import models.people.AdministrativeWorker;
import models.people.ResearcherLecturer;
import tools.CheckProperTypes;
import tools.Alerts;

public class EmployeeCreationController {

    public Label nameNSurname;
    public TextField nameNSurnameInput;
    public Label age;
    public TextField ageInput;
    public Label pesel;
    public TextField peselInput;
    public Label department;
    public TextField departmentInput;
    public Label wage;
    public TextField wageInput;
    public ChoiceBox<String> isLecturer;
    public Label subject;
    public TextField subjectInput;
    public Button submitButton;

    private String employeesName;
    private String employeesSurname;
    private int employeesAge;
    private String employeesPesel;
    private String employeesDepartment;
    private double employeesWage;
    private String lecturersSubject;

    //when user is finished providing necessary data, checks if said is correct then creates and adds
    //right employee to universities list
    public void submitData() {

        if (checkAll()) {

            if (isLecturer.getValue().equals("Lecturer")) {

                UniversitySimulator.runningUniversity.addEmployee(new ResearcherLecturer(employeesName, employeesSurname, employeesAge, employeesPesel, employeesDepartment, employeesWage, lecturersSubject));

            } else if (isLecturer.getValue().equals("Administrative Worker")) {

                UniversitySimulator.runningUniversity.addEmployee(new AdministrativeWorker(employeesName, employeesSurname, employeesAge, employeesPesel, employeesDepartment, employeesWage));

            } else {

                Alert noChoiceAfterCheck = new Alert(Alert.AlertType.ERROR);
                noChoiceAfterCheck.setTitle("Unexpected error");
                noChoiceAfterCheck.setHeaderText("Unfortunately something went wrong with employee type you provided. Please check your choice.");
                noChoiceAfterCheck.showAndWait();

            }

            ((Stage) submitButton.getScene().getWindow()).close();

        }
    }

    //checks whether all fields are filled with correct data, otherwise highlights wrong ones red
    private boolean checkAll() {

        boolean allGood = true;

        if (nameNSurnameInput.getText().isEmpty() || nameNSurnameInput.getText().split("\\s+").length != 2) {

            Alerts.addRedAlert(nameNSurnameInput);
            allGood = false;

            nameNSurnameInput.clear();
            nameNSurnameInput.setPromptText("It has to consist of two words");

        } else {

            String[] employeesNameNSurname = nameNSurnameInput.getText().split("\\s+");
            employeesName = employeesNameNSurname[0];
            employeesSurname = employeesNameNSurname[1];

        }

        if (ageInput.getText().isEmpty()) {

            Alerts.addRedAlert(ageInput);
            allGood = false;

        } else {

            employeesAge = CheckProperTypes.getInteger(ageInput, 21, 100);

            if (employeesAge == -1) {
                allGood = false;
            }

        }

        if (peselInput.getText().isEmpty() || peselInput.getText().length() != 11 || !CheckProperTypes.checkPesel(peselInput.getText())) {
            Alerts.addRedAlert(peselInput);
            allGood = false;
        } else {
            employeesPesel = peselInput.getText();
        }

        if (departmentInput.getText().isEmpty()) {
            Alerts.addRedAlert(departmentInput);
            allGood = false;
        } else {
            employeesDepartment = departmentInput.getText();
        }

        if (wageInput.getText().isEmpty()) {

            Alerts.addRedAlert(wageInput);
            allGood = false;

        } else {

            employeesWage = CheckProperTypes.getWage(wageInput);

            if (employeesWage == -1) {
                allGood = false;
            }

        }

        if (isLecturer.getValue() == null) {

            if (!isLecturer.getStyleClass().contains("error")) {

                if (!isLecturer.getScene().getStylesheets().contains(getClass().getClassLoader().getResource("views/styles/text-input-red-border.css"))) {
                    isLecturer.getScene().getStylesheets().add(getClass().getClassLoader().getResource("views/styles/text-input-red-border.css").toExternalForm());
                }

                isLecturer.getStyleClass().add("error");

            }

            allGood = false;

        } else if (isLecturer.getValue().equals("Lecturer") && subjectInput.getText().isEmpty()) {

            Alerts.addRedAlert(subjectInput);
            allGood = false;

        } else {

            lecturersSubject = subjectInput.getText();

        }

        return allGood;
    }

    //methods for each textfield to remove red glow on click
    public void resetRedAlertNameNSurname() {
        Alerts.resetRedAlert(nameNSurnameInput);
    }

    public void resetRedAlertAge() {
        Alerts.resetRedAlert(ageInput);
    }

    public void resetRedAlertPesel() {
        Alerts.resetRedAlert(peselInput);
    }

    public void resetRedAlertDepartment() {
        Alerts.resetRedAlert(departmentInput);
    }

    public void resetRedAlertWage() {
        Alerts.resetRedAlert(wageInput);
    }

    //resets red alert at isLecturer choice, if Lecturer is entered uncovers subject field
    public void choiceEntered() {

        if (isLecturer.getStyleClass().contains("error")) {
            isLecturer.getStyleClass().remove("error");
        }

        if (isLecturer.getValue() != null) {//if entered should always be

            if (isLecturer.getValue().equals("Lecturer")) {

                subject.setVisible(true);
                subjectInput.setVisible(true);
                subject.setDisable(false);
                subjectInput.setDisable(false);

            } else if (isLecturer.getValue().equals("Administrative Worker")) {

                subject.setVisible(false);
                subjectInput.setVisible(false);
                subject.setDisable(true);
                subjectInput.setDisable(true);

            }

        }

    }

    public void resetRedAlertSubject() {
        Alerts.resetRedAlert(subjectInput);
    }

}
