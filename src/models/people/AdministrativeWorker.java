package models.people;

public class AdministrativeWorker extends UniversityEmployee {

    private static final long serialVersionUID = 4134992833296507962L;

    public AdministrativeWorker(final String employeesName, final String employeesSurname, final int employeesAge, final String employeesPesel, final String employeesDepartment, final double employeesWage) {

        super(employeesName, employeesSurname, employeesAge, employeesPesel, employeesDepartment, employeesWage);

    }

}
