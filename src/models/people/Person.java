package models.people;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.Serializable;

public abstract class Person implements Serializable {

    private static final long serialVersionUID = -1746493138291296660L;
    private String name;
    private String surname;
    private int age;
    private String pesel;
    private int happinessStatus;

    public Person(final String personsName, final String personsSurname, final int personsAge, final String personsPesel) {

        name = personsName;
        surname = personsSurname;
        age = personsAge;
        pesel = personsPesel;
        happinessStatus = calcInitialHappiness();

    }


    //GETTERS
    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getFullName() {
        return name + " " + surname;
    }

    public int getAge() {
        return age;
    }

    public String getPesel() {
        return pesel;
    }

    public int getHappinessStatus() {
        return happinessStatus;
    }

    //depending on happiness Status returns different visual representations of current happiness level
    public ImageView getHappinessLevel() {

        ImageView happinessLevel;

        if (happinessStatus < 33) {

            happinessLevel = new ImageView(new Image("res/sadface.png"));

        } else if (happinessStatus < 66) {

            happinessLevel = new ImageView(new Image("res/neutralface.png"));

        } else {

            happinessLevel = new ImageView(new Image("res/smileyface.png"));

        }

        happinessLevel.setFitHeight(50);
        happinessLevel.setFitWidth(50);
        return happinessLevel;
    }


    //SETTERS
    public void setName(final String personsName) {
        name = personsName;
    }

    public void setSurname(final String personsSurname) {
        surname = personsSurname;
    }

    public void setAge(final int personsAge) {
        age = personsAge;
    }

    public void setHappinessStatus(final int happinessStatusToSet) {
        happinessStatus = happinessStatusToSet;
    }


    //HAPPINESS
    //both employees and students have initial happiness, but its calculated differently
    public abstract int calcInitialHappiness();

}
