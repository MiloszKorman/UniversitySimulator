package models.people;

import models.activities.Consultations;
import models.activities.Organization;
import models.happiness.HappinessBasedOnConsultations;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public final class Student extends Person implements HappinessBasedOnConsultations, Observer {

    private static final long serialVersionUID = 5547716225815365219L;
    private ArrayList<Consultations> consultationsIAttend = new ArrayList<>();
    private ArrayList<Organization> organizationsImSignedUpFor = new ArrayList<>();
    private String albumNumber;

    public Student(final String name, final String surname, final int age, final String pesel, final String studentsAlbumNumber) {

        super(name, surname, age, pesel);
        albumNumber = studentsAlbumNumber;

    }


    //GETTERS
    public String getStudentsAlbumNumber() {
        return albumNumber;
    }

    public ArrayList<Consultations> getConsultationsIAttend() {
        return consultationsIAttend;
    }

    public ArrayList<Organization> getOrganizationsImSignedUpFor() {
        return organizationsImSignedUpFor;
    }


    //METHODS FOR STUDENT TO MANAGE HIS CONSULTATIONS
    public void addConsultations(final Consultations newConsultations) {
        consultationsIAttend.add(newConsultations);
    }

    public void removeConsultations(final Consultations consultationsToRemove) {
        consultationsIAttend.remove(consultationsToRemove);
    }


    //METHODS FOR STUDENT TO MANAGE HIS ORGANIZATIONS
    public void addOrganization(final Organization newOrganization) {
        organizationsImSignedUpFor.add(newOrganization);
    }

    public void removeOrganization(final Organization organizationToRemove) {
        organizationsImSignedUpFor.remove(organizationToRemove);
    }


    //HAPPINESS
    //on default every student starts neutrally with 50% happiness status
    @Override
    public int calcInitialHappiness() {
        return 50;
    }

    //when something changes with organizations or consultations student is signed up for, it changes his
    //happiness status
    @Override
    public void updateHappiness(int impact) {

        int happinessStatus = getHappinessStatus();

        happinessStatus += impact;

        if (100 < happinessStatus) {
            happinessStatus = 100;
        } else if (happinessStatus < 1) {
            happinessStatus = 1;
        }

        setHappinessStatus(happinessStatus);

    }

    //used by organizations when something changes within them
    @Override
    public void update(Observable o, Object arg) {
        updateHappiness(((Integer) arg).intValue());
    }
}
