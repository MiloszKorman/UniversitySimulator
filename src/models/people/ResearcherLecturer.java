package models.people;

import GUI.UniversitySimulator;
import javafx.scene.image.ImageView;
import models.activities.Consultations;
import models.activities.Organization;
import models.happiness.HappinessBasedOnConsultations;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public final class ResearcherLecturer extends UniversityEmployee implements HappinessBasedOnConsultations, Observer {

    private static final long serialVersionUID = 3811571671315256150L;
    private String subject;
    private ArrayList<Consultations> myConsultations = new ArrayList<>();
    private ArrayList<Organization> myOrganizations = new ArrayList<>();

    public ResearcherLecturer(final String lecturersName, final String lecturersSurname, final int lecturersAge, final String lecturersPesel, final String lecturersDepartment, final double lecturersWage, final String lecturersSubject) {

        super(lecturersName, lecturersSurname, lecturersAge, lecturersPesel, lecturersDepartment, lecturersWage);
        subject = lecturersSubject;

    }


    //GETTERS
    public String getSubject() {
        return subject;
    }

    public int getNumberOfConsultations() {
        return myConsultations.size();
    }

    public ArrayList<Consultations> getMyConsultations() {
        return myConsultations;
    }

    public ArrayList<Organization> getMyOrganizations() {
        return myOrganizations;
    }

    //runs through all consultations where happiness might be influenced, then proceeds with method in person
    //starts with initial happiness that is based on wage and influenced by organizations
    @Override
    public ImageView getHappinessLevel() {

        setHappinessStatus(calcInitialHappiness());

        for (Consultations consultation : myConsultations) {

            int timeLeft = consultation.getTimeLeft();
            int duration = consultation.getDuration();

            if (timeLeft < (duration / 4)) {

                updateHappiness(-10);

            } else if (((3 * duration) / 4) < timeLeft) {

                updateHappiness(10);

            }

        }

        return super.getHappinessLevel();
    }


    //SETTERS
    public void setSubject(final String lecturersSubject) {
        subject = lecturersSubject;
    }


    //METHODS FOR LECTURER TO MANAGE HIS CONSULTATIONS
    public void addConsultation(final Consultations consultationToAdd) {
        myConsultations.add(consultationToAdd);
    }

    //everyone is laid off and consultation is removed from lecturers list
    public void removeConsultation(final Consultations consultationToRemove) {
        consultationToRemove.layAllOff();
        myConsultations.remove(consultationToRemove);
    }


    //METHODS FOR LECTURER TO MANAGE HIS ORGANIZATIONS
    public void addOrganization(final Organization organizationToAdd) {
        myOrganizations.add(organizationToAdd);
    }

    //everyone is laid off, organization is removed from lecturers list, as well as Universities list
    public void removeOrganization(final Organization organizationToRemove) {
        organizationToRemove.layAllOff();
        myOrganizations.remove(organizationToRemove);
        UniversitySimulator.runningUniversity.removeOrganization(organizationToRemove);
    }


    //HAPPINESS
    @Override
    public void updateHappiness(int impact) {

        int happinessStatus = getHappinessStatus();

        happinessStatus += impact;

        if (100 < happinessStatus) {

            happinessStatus = 100;

        } else if (happinessStatus < 1) {

            happinessStatus = 1;

        }

        setHappinessStatus(happinessStatus);

    }

    //since consultations are calculated when happiness is checked, in lecturer impact from organizations
    //adds to initial which is then operated on with consultations when happiness is checked
    @Override
    public void update(Observable o, Object arg) {
        changeInitialHappinessDifference(((Integer) arg).intValue());
    }
}
