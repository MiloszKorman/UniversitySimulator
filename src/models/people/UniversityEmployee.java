package models.people;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public abstract class UniversityEmployee extends Person {

    private static final long serialVersionUID = -751113770670905812L;
    private String department;
    private double wage;
    private int initialHappinessDifference = 0;


    UniversityEmployee(final String employeesName, final String employeesSurname, final int employeesAge, final String employeesPesel, final String employeesDepartment, final double employeesWage) {

        super(employeesName, employeesSurname, employeesAge, employeesPesel);
        department = employeesDepartment;
        wage = employeesWage;
        //hence for employee initial happiness might be calculated correctly only after wage assignment
        super.setHappinessStatus(calcInitialHappiness());

    }


    //GETTERS
    public String getDepartment() {
        return department;
    }

    public double getWage() {
        return wage;
    }

    public ImageView getIsLecturer() {

        ImageView isLecturer;

        if (this instanceof ResearcherLecturer) {
            isLecturer = new ImageView(new Image("res/yes.png"));
        } else {
            isLecturer = new ImageView(new Image("res/no.png"));
        }

        isLecturer.setFitWidth(50);
        isLecturer.setFitHeight(50);

        return isLecturer;
    }


    //SETTERS
    public void setDepartment(final String employeesDepartment) {
        department = employeesDepartment;
    }

    public void setWage(double employeesWage) {

        //wage changes, so baseline happiness changes as well
        int difference = getHappinessStatus() - calcInitialHappiness();
        wage = employeesWage;
        int happinessStatus = calcInitialHappiness() + difference;

        if (100 < happinessStatus) {
            happinessStatus = 100;
        } else if (happinessStatus < 1) {
            happinessStatus = 1;
        }

        setHappinessStatus(happinessStatus);

    }


    //HAPPINESS
    //changes based on organizations activities
    void changeInitialHappinessDifference(final int change) {
        initialHappinessDifference += change;
    }

    //return initial happiness that is based on employees wage
    @Override
    public int calcInitialHappiness() {

        int happiness = ((int) getWage() - 1000) / 100;

        happiness += initialHappinessDifference;

        if (happiness < 1) {
            happiness = 1;
        } else if (100 < happiness) {
            happiness = 100;
        }

        return happiness;
    }

}
