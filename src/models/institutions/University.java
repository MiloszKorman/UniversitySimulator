package models.institutions;

import java.io.*;
import java.util.ArrayList;

import models.activities.Consultations;
import models.activities.Organization;
import models.people.*;

public final class University implements Serializable {

    private static final long serialVersionUID = -6746741252882327509L;
    private String name;
    private ArrayList<UniversityEmployee> listOfEmployees = new ArrayList<>();
    private ArrayList<Student> listOfStudents = new ArrayList<>();
    private ArrayList<Organization> listOfOrganizations = new ArrayList<>();

    public University(String institutionsName) {

        name = institutionsName;

    }


    //GETTERS
    public String getName() {
        return name;
    }

    public ArrayList<Student> getListOfStudents() {
        return listOfStudents;
    }

    public ArrayList<UniversityEmployee> getListOfEmployees() {
        return listOfEmployees;
    }

    public ArrayList<Organization> getListOfOrganizations() {
        return listOfOrganizations;
    }


    //METHODS TO MANAGE EMPLOYEES
    public void addEmployee(final UniversityEmployee employeeToAdd) {
        listOfEmployees.add(employeeToAdd);
    }

    //on employee deletion if said is lecturer, all his consultation hours and organizations are as well removed
    public void removeEmployee(final UniversityEmployee employeeToRemove) {

        if(employeeToRemove instanceof ResearcherLecturer) {

            ResearcherLecturer lecturerToRemove = (ResearcherLecturer) employeeToRemove;

            for(Consultations consultationToRemove : lecturerToRemove.getMyConsultations()) {

                lecturerToRemove.removeConsultation(consultationToRemove);

            }

            for(Organization organizationToRemove : lecturerToRemove.getMyOrganizations()) {

                lecturerToRemove.removeOrganization(organizationToRemove);

            }

        }

        listOfEmployees.remove(employeeToRemove);

    }


    //METHODS TO MANAGE STUDENTS
    public void addStudent(final Student studentToAdd) {
        listOfStudents.add(studentToAdd);
    }

    public void removeStudent(final Student studentToRemove) {


        listOfStudents.remove(studentToRemove);
    }


    //METHODS TO MANAGE ORGANIZATIONS
    public void addOrganization(final Organization organizationToAdd) {
        listOfOrganizations.add(organizationToAdd);
    }

    public void removeOrganization(final Organization organizationToRemove) {
        listOfOrganizations.remove(organizationToRemove);
    }

}
