package models.activities;

import java.io.Serializable;

public class Post implements Serializable {

    private static final long serialVersionUID = 1036660892750436092L;
    private String title;
    private String message;
    private int impact;

    public Post(final String titleProvided, final String messageProvided, final int impactProvided) {

        title = titleProvided;
        message = messageProvided;
        impact = impactProvided;

    }


    //GETTERS
    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public int getImpact() {
        return impact;
    }

}
