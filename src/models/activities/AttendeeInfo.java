package models.activities;

import models.people.Student;

import java.io.Serializable;

public final class AttendeeInfo implements Serializable {

    private static final long serialVersionUID = -7709615896038986591L;
    private int studentsPriority;
    private int studentsTime = 0;
    private int studentsMinTime;
    private int studentsMaxTime;
    private Consultations consultationOwner;

    public AttendeeInfo(final int priority, final int minTime, final int maxTime, final Consultations owner) {

        studentsPriority = priority;
        studentsMinTime = minTime;
        studentsMaxTime = maxTime;
        consultationOwner = owner;

        setTime();

    }


    //GETTERS
    public Consultations getConsultationsOwner() {
        return consultationOwner;
    }

    public int getPriority() {
        return studentsPriority;
    }

    public int getMyTime() {
        return studentsTime;
    }

    public int getMinTime() {
        return studentsMinTime;
    }

    public int getMaxTime() {
        return studentsMaxTime;
    }


    //SETTERS
    //at first student unsubscribes to this consultation, then tries to sign up with new requirements(new priority)
    public void setNewPriority(final int newPriority, final Student studentToChange) {

        consultationOwner.removeStudent(studentToChange);
        studentToChange.removeConsultations(consultationOwner);
        Consultations editedConsultation = consultationOwner.singUp(studentToChange, newPriority, studentsMinTime, studentsMaxTime);

        //no Allocation is evoked on student second time (first at sign up in consultation), which means
        //double the normal impact, because he screwed up, which influences his happiness greatly
        if (editedConsultation == null) {
            studentToChange.updateHappiness(-5);
        } else {
            studentToChange.addConsultations(editedConsultation);
            studentToChange.updateHappiness(5);
        }

    }

    //at first student unsubscribes to this consultation, then tries to sign up with new requirements(new min time)
    public void setNewMinTime(final int minTime, final Student studentToChange) {

        consultationOwner.removeStudent(studentToChange);
        studentToChange.removeConsultations(consultationOwner);
        Consultations editedConsultation = consultationOwner.singUp(studentToChange, studentsPriority, minTime, studentsMaxTime);

        //no Allocation is evoked on student second time (first at sign up in consultation), which means
        //double the normal impact, because he screwed up, which influences his happiness greatly
        if (editedConsultation == null) {
            studentToChange.updateHappiness(-5);
        } else {
            studentToChange.addConsultations(editedConsultation);
            studentToChange.updateHappiness(5);
        }

    }

    //at first student unsubscribes to this consultation, then tries to sign up with new requirements(new max time)
    public void setNewMaxTime(final int maxTime, final Student studentToChange) {

        consultationOwner.removeStudent(studentToChange);
        studentToChange.removeConsultations(consultationOwner);
        Consultations editedConsultation = consultationOwner.singUp(studentToChange, studentsPriority, studentsMinTime, maxTime);

        //no Allocation is evoked on student second time (first at sign up in consultation), which means
        //double the normal impact, because he screwed up, which influences his happiness greatly
        if (editedConsultation == null) {
            studentToChange.updateHappiness(-5);
        } else {
            studentToChange.addConsultations(editedConsultation);
            studentToChange.updateHappiness(5);
        }

    }

    //used to decide how much time can be allocated for certain student
    private void setTime() {

        if (studentsPriority == 3) {

            if (studentsMaxTime <= consultationOwner.getTimeLeft()) {
                studentsTime = studentsMaxTime;
            } else if (studentsMinTime <= consultationOwner.getTimeLeft()) {
                studentsTime = consultationOwner.getTimeLeft();
            } else {
                studentsTime = consultationOwner.rearrangeStudents(studentsMinTime);
            }

        } else if (studentsPriority == 2) {

            if (studentsMaxTime <= consultationOwner.getTimeLeft()) {
                studentsTime = studentsMaxTime;
            } else if (studentsMinTime <= consultationOwner.getTimeLeft()) {
                studentsTime = consultationOwner.getTimeLeft();
            }

        } else if (studentsPriority == 1) {

            if (studentsMinTime <= consultationOwner.getTimeLeft()) {
                studentsTime = studentsMinTime;
            }

        }

    }


    //evoked on each priority 2 student, when student with priority 3 comes, and there is not enough time
    public int reduceToMin() {

        int difference = studentsTime - studentsMinTime;
        studentsTime = studentsMinTime;

        return difference;
    }

}
