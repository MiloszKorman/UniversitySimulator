package models.activities;

import models.people.ResearcherLecturer;
import models.people.Student;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;

public class Organization extends Observable implements Serializable {

    private static final long serialVersionUID = 7616534461579453897L;

    private ArrayList<Post> listOfPosts = new ArrayList<>();
    private String name;
    private String field;
    private ResearcherLecturer lecturerOwner;
    private ArrayList<Student> listOfStudents = new ArrayList<>();

    public Organization(final String organizationsName, final String organizationsField, final ResearcherLecturer organizationsOwner) {

        name = organizationsName;
        field = organizationsField;
        lecturerOwner = organizationsOwner;
        addObserver(lecturerOwner);

    }


    //GETTERS
    public ResearcherLecturer getLecturerOwner() {
        return lecturerOwner;
    }

    public String getName() {
        return name;
    }

    public String getField() {
        return field;
    }

    public ArrayList<Post> getListOfPosts() {
        return listOfPosts;
    }

    //STUDENTS MANAGEMENT
    public void signStudentUp(final Student studentToSignUp) {
        listOfStudents.add(studentToSignUp);
        addObserver(studentToSignUp);
    }

    public void letMeResign(final Student studentResigning) {
        listOfStudents.remove(studentResigning);
        deleteObserver(studentResigning);
    }

    //updates -25 on every observer happiness and signs them off
    public void layAllOff() {

        setChanged();
        notifyObservers(new Integer(-25));

        for (Student studentToLayOff : listOfStudents) {
            studentToLayOff.removeOrganization(this);
        }

        deleteObservers();

    }


    //POSTS MANAGEMENT
    //adds new post to list of them, and updates observers happiness accordingly to posts impact
    public void addPost(final Post postToAdd) {

        setChanged();
        notifyObservers(new Integer(postToAdd.getImpact()));

        listOfPosts.add(postToAdd);

    }

}
