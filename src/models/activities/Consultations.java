package models.activities;

import java.io.Serializable;
import java.util.*;

import models.people.ResearcherLecturer;
import models.people.Student;

public final class Consultations implements Serializable {

    private static final long serialVersionUID = 9086002791446880001L;
    private String day;
    private int hour;
    private int minute;
    private int duration;   //in minutes
    private int timeLeft;   //in minutes
    private ResearcherLecturer lecturerOwner;
    private HashMap<Student, AttendeeInfo> studentsSignedUp = new HashMap<>();   //key is the student object, value AttendeeInfo object, which stores his time and priority

    public Consultations(final String dayProvided, final int hourProvided, final int minuteProvided, final int durationProvided, final ResearcherLecturer lecturerOwnerProvided) {

        day = dayProvided;
        hour = hourProvided;
        minute = minuteProvided;
        duration = durationProvided;
        timeLeft = duration;
        lecturerOwner = lecturerOwnerProvided;

    }


    //GETTERS
    public ResearcherLecturer getLecturerOwner() {
        return lecturerOwner;
    }

    public AttendeeInfo getMyAttendeeInfo(Student studentDemandingHisInfo) {
        return studentsSignedUp.get(studentDemandingHisInfo);
    }

    public String getDay() {
        return day;
    }

    public String getTime() {
        return hour + ":" + minute;
    }

    public int getDuration() {
        return duration;
    }

    public int getTimeLeft() {
        return timeLeft;
    }

    public int getNumberOfStudents() {
        return studentsSignedUp.size();
    }


    //SETTERS
    public void setDay(final String dayProvided) {
        day = dayProvided;
    }

    public void setTime(final int hourProvided, final int minuteProvided) {
        hour = hourProvided;
        minute = minuteProvided;
    }

    //when time is changed, and new duration is smaller than time required for all students, some students must be laid
    //off, first off first priority students are laid off, later second, and finally third ones
    public void setDuration(final int durationProvided) {

        Iterator<Student> listOfStudents = studentsSignedUp.keySet().iterator();

        int priorityToGo = 1;

        while (durationProvided < duration - timeLeft) {

            while (listOfStudents.hasNext()) {

                if (studentsSignedUp.get(listOfStudents.next()).getPriority() == priorityToGo) {
                    listOfStudents.next().updateHappiness(-25);
                    removeStudent(listOfStudents.next());
                }

            }

            listOfStudents = studentsSignedUp.keySet().iterator();
            priorityToGo++;

        }

        int requestTime = duration - timeLeft;
        duration = durationProvided;
        timeLeft = durationProvided - requestTime;

    }


    //STUDENTS MANAGEMENT
    //if at least students minimal time requirements are met, he is added as an attendee and time for him is
    //subtracted from overall left time
    public Consultations singUp(final Student newVisitor, final int priority, final int minTime, final int maxTime) {

        if (!studentsSignedUp.containsKey(newVisitor) && notYetFull()) {

            AttendeeInfo newVisitorsInfo = new AttendeeInfo(priority, minTime, maxTime, this);
            int newVisitorsTime = newVisitorsInfo.getMyTime();
            if (0 < newVisitorsTime) {

                studentsSignedUp.put(newVisitor, newVisitorsInfo);
                timeLeft -= newVisitorsTime;

                if (newVisitorsTime == maxTime) {
                    newVisitor.updateHappiness(10);
                } else if (newVisitorsTime == minTime) {
                    newVisitor.updateHappiness(2);
                } else {
                    newVisitor.updateHappiness(5);
                }

                return this;

            } else {

                newVisitor.updateHappiness(-5);

            }

        }

        return null;
    }

    //if student is signed up, then adds time that was assigned to him to time left, and removes him
    public Consultations removeStudent(final Student backingOut) {

        if (studentsSignedUp.containsKey(backingOut)) {
            timeLeft += studentsSignedUp.get(backingOut).getMyTime();
            studentsSignedUp.remove(backingOut);
        }

        return this;
    }

    //goes through list of all students signed up, then writes them out, their happiness is also impacted
    public void layAllOff() {

        for (Student signedUp : studentsSignedUp.keySet()) {

            signedUp.removeConsultations(this);
            signedUp.updateHappiness(-25);

        }

    }

    //evoked when priority 3 student comes, and there is not enough time for him
    public int rearrangeStudents(final int neededTime) {

        Iterator<Student> students = studentsSignedUp.keySet().iterator();

        while (students.hasNext() && timeLeft < neededTime) {

            Student nextStudent = students.next();

            if (studentsSignedUp.get(nextStudent).getPriority() == 2) {

                if (studentsSignedUp.get(nextStudent).getMinTime() < studentsSignedUp.get(nextStudent).getMyTime()) {
                    timeLeft += studentsSignedUp.get(nextStudent).reduceToMin();
                    nextStudent.updateHappiness(-20);
                }

            }

        }

        return timeLeft;
    }

    private boolean notYetFull() {

        return (0 < timeLeft);
    }

}
