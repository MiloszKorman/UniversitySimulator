package models.happiness;

public interface HappinessBasedOnConsultations {

    void updateHappiness(int impact);

}
