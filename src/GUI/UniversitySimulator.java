package GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import models.activities.Consultations;
import models.activities.Organization;
import models.activities.Post;
import models.institutions.University;
import models.people.Person;
import tools.Saves;

public class UniversitySimulator extends Application {

    public static Stage mainWindow;

    public static University runningUniversity = null;
    public static Person awaitingBeingManaged = null;
    public static Consultations consultationBuffer = null;
    public static Organization organizationBuffer = null;
    public static Post postBuffer = null;

    public static void main(String[] args) {

        launch(args);

    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent welcome = FXMLLoader.load(getClass().getResource("../views/welcome-screen.fxml"));
        mainWindow = primaryStage;
        primaryStage.setTitle("University Simulator 1.0");
        primaryStage.setScene(new Scene(welcome));
        primaryStage.setMinWidth(1152);
        primaryStage.setMinHeight(648);

        primaryStage.setOnCloseRequest(e -> {

            e.consume();
            closeProgram();

        });

        primaryStage.show();

    }

    //when user tries to close the program, he has an option to save his progress
    private void closeProgram() {

        if (runningUniversity == null || runningUniversity.getName().isEmpty()) {
            mainWindow.close();
        } else {

            Stage exiting = new Stage();
            exiting.setTitle("Exit");

            VBox externalLayout = new VBox();
            externalLayout.setAlignment(Pos.CENTER);
            externalLayout.setPadding(new Insets(8, 8, 8, 8));
            externalLayout.setSpacing(16);
            HBox internalLayout = new HBox();
            internalLayout.setAlignment(Pos.CENTER);
            internalLayout.setSpacing(8);
            Label info = new Label("Do you want to save before closing?");
            Button saveAndClose = new Button("Yes");
            Button justClose = new Button("No");

            saveAndClose.setOnAction(e -> {

                Saves.saveEntry(UniversitySimulator.runningUniversity.getName());
                justClose.fire();

            });

            justClose.setOnAction(e -> {

                exiting.close();
                mainWindow.close();

            });

            internalLayout.getChildren().addAll(saveAndClose, justClose);
            externalLayout.getChildren().addAll(info, internalLayout);
            exiting.setScene(new Scene(externalLayout, 300, 100));
            exiting.showAndWait();

        }

    }

}
