# UniversitySimulator

Java project for laboratories. The premise is to create a somewhat simulator of an university. The main features are consultation hours and organizations. They belong to professors and students can sign up for them.

Organizations posts(with help of observer pattern) and time assigned for student in a consultation both influence his happiness level.<br />
Employees happiness level is based on their wage.<br />
Lecturers happiness level is based on his wage as well, but also his organizations posts(with help of observer pattern) and how much time does he have to spend during consultations.
